package by.epam.tatoosalon.validator;

public interface ValidatorRegistration {
    boolean validData();
}

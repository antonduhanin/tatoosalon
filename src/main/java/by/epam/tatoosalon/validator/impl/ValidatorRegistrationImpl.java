package by.epam.tatoosalon.validator.impl;

import by.epam.tatoosalon.validator.ValidatorRegistration;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ValidatorRegistrationImpl implements ValidatorRegistration {
    private final String login;
    private final String confirmLogin;
    private final String password;
    private final String confirmPassword;
    private static final String EMAIL_REGEX
            = "^[a-zA-Z0-9_!#$%&'*+/=?`{|}~^.-]+@[a-zA-Z0-9.-]+$";

    public ValidatorRegistrationImpl(String login, String confirmLogin,
                                     String password, String confirmPassword) {
        this.login = login;
        this.confirmLogin = confirmLogin;
        this.password = password;
        this.confirmPassword = confirmPassword;
    }


    @Override
    public boolean validData() {
        if (login != null && confirmLogin != null
                && password != null && confirmPassword != null) {
            return validateLogin()
                    && validatePassword();
        }
        return false;
    }

    private boolean validatePassword() {
        if (password.equals(confirmPassword) && !password.equals("")) {
            return password.length() < 255;
        }
        return false;
    }


    private boolean validateLogin() {
        if (login.equals(confirmLogin) && !login.equals("")) {
            Pattern pattern = Pattern.compile(EMAIL_REGEX);
            Matcher matcher = pattern.matcher(login);
            return login.length() < 255 && matcher.matches();
        }
        return false;
    }

}

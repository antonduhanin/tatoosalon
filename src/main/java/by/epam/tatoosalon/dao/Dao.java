package by.epam.tatoosalon.dao;

import by.epam.tatoosalon.entity.BaseObj;
import by.epam.tatoosalon.exception.DaoException;

import java.util.List;

public interface Dao<Type extends BaseObj> {
    Integer create(Type entity) throws DaoException;

    Type read(int id) throws DaoException;

    void update(Type entity) throws DaoException;

    void delete(int id) throws DaoException;

    List<Type> getAll() throws DaoException;
}

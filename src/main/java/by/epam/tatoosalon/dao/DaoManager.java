package by.epam.tatoosalon.dao;

import by.epam.tatoosalon.exception.DaoException;

import java.sql.Connection;

public interface DaoManager {
    <Type extends Dao<?>> Type createDao(Class<Type> key) throws DaoException;

    void commit() throws DaoException;

    void rollback() throws DaoException;

    void setTransaction(boolean transaction) throws DaoException;

    void openConnection() throws DaoException;

    void closeConnection() throws DaoException;

    boolean isTransaction() throws DaoException;
}

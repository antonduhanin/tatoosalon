package by.epam.tatoosalon.dao;

import by.epam.tatoosalon.entity.pojo.Tattoo;
import by.epam.tatoosalon.exception.DaoException;

public interface TattooDao extends Dao<Tattoo> {

    void rateWork(int tatooId, int orderId, int rateValue) throws DaoException;

    void rateTattoo(int tatooId, int userId, int rateValue) throws DaoException;

    void deleteRatingTattoo(int tatooId, int userId) throws DaoException;

    void deleteRatingWork(int tatooId, int orderId) throws DaoException;

}

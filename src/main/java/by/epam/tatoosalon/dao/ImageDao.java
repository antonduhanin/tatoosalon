package by.epam.tatoosalon.dao;

import by.epam.tatoosalon.entity.pojo.Image;
import by.epam.tatoosalon.exception.DaoException;

import java.util.List;

public interface ImageDao extends Dao<Image> {
    List<Image> getImageByTattoo(int tattooId) throws DaoException;
}

package by.epam.tatoosalon.dao;

import by.epam.tatoosalon.entity.pojo.User;
import by.epam.tatoosalon.exception.DaoException;

public interface UserDao extends Dao<User> {
    User read(String login, String password) throws DaoException;

    User readByLogin(String login) throws DaoException;
}

package by.epam.tatoosalon.dao;

import by.epam.tatoosalon.entity.pojo.Role;
import by.epam.tatoosalon.exception.DaoException;

public interface RoleDao extends Dao<Role> {
    Role readByName(String name) throws DaoException;
}

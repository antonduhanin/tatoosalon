package by.epam.tatoosalon.dao;

import by.epam.tatoosalon.entity.pojo.Discount;
import by.epam.tatoosalon.exception.DaoException;

import java.util.Date;
import java.util.List;

public interface DiscountDao extends Dao<Discount> {
    List<Discount> findDiscountsForUserId(int id) throws DaoException;

    void setDiscountForUserId(int discountId, int userId, Date date)
            throws DaoException;

    List<Discount> findActiveDiscountsForUserId(int userId) throws DaoException;

    void deleteDiscountForUserId(int userId) throws DaoException;
}

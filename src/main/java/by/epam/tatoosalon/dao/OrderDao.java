package by.epam.tatoosalon.dao;

import by.epam.tatoosalon.entity.pojo.Order;
import by.epam.tatoosalon.entity.pojo.WorkInfo;
import by.epam.tatoosalon.exception.DaoException;

import java.util.List;

public interface OrderDao extends Dao<Order> {
    List<Order> findOrdersByUser(int userId) throws DaoException;

    void addTattooInOrder(WorkInfo workInfo)
            throws DaoException;

    void deleteTattooInOrder(WorkInfo workInfo)
            throws DaoException;
}

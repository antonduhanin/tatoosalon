package by.epam.tatoosalon.dao.pool;

import by.epam.tatoosalon.exception.DaoException;

import java.sql.Connection;
import java.sql.SQLException;

public interface ConnectionPool {
    void checkIn(Connection connection) throws DaoException;

    Connection checkOut() throws DaoException;
}

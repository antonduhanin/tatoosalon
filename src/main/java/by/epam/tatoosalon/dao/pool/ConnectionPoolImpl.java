package by.epam.tatoosalon.dao.pool;

import by.epam.tatoosalon.exception.DaoException;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Deque;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Semaphore;
import java.util.concurrent.locks.ReentrantLock;

import static by.epam.tatoosalon.dao.constant.DaoConstants.PROPERTIES_FILE;

public class ConnectionPoolImpl implements ConnectionPool {
    private static final ReentrantLock LOCKER = new ReentrantLock();
    private static ConnectionPool instance;
    private Semaphore semaphore;
    private Deque<Connection> connections;
    private String url;
    private String user;
    private String password;
    private int poolSize;

    private ConnectionPoolImpl() throws DaoException {
        try {
            initProperties();
            DriverManager.registerDriver(new com.mysql.jdbc.Driver());
            connections = new LinkedList<>();
            semaphore = new Semaphore(poolSize, true);
        } catch (IOException | SQLException e1) {
            throw new DaoException(e1.getMessage());
        }
    }

    public static ConnectionPool getInstance() throws DaoException {
        if (instance == null) {
            LOCKER.lock();
            if (instance == null) {
                instance = new ConnectionPoolImpl();
            }
            LOCKER.unlock();
        }
        return instance;
    }

    @Override
    public void checkIn(Connection connection) {
        try {
            LOCKER.lock();
            connections.add(connection);
        } finally {
            LOCKER.unlock();
            semaphore.release();
        }
    }

    @Override
    public Connection checkOut() {
        Connection connection;
        try {
            semaphore.acquire();
            LOCKER.lock();
            int currentPoolSize = connections.size();
            if (currentPoolSize == 0) {
                connection = getConnection();
            } else {
                connection = connections.pop();
            }
            return connection;
        } catch (SQLException | InterruptedException e) {
            throw new IllegalThreadStateException(e.getMessage());
        } finally {
            LOCKER.unlock();
        }
    }

    private Connection getConnection() throws SQLException {
        return DriverManager.getConnection(url, user, password);
    }

    private void initProperties() throws IOException {
        InputStream inputStream = ConnectionPoolImpl.class
                .getResourceAsStream(PROPERTIES_FILE);
        Properties properties = new Properties();
        properties.load(inputStream);
        url = (String) properties.get("URL");
        user = (String) properties.get("USER");
        password = (String) properties.get("PASSWORD");
        poolSize = Integer.parseInt((String) properties.get("SIZE"));
    }
}

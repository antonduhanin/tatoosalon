package by.epam.tatoosalon.dao.mysql.constant;

public class RoleQueries {
    public static final String GET_ALL_ROLES
            = "SELECT id, name FROM role";
    public static final String SELECT_BY_ID_QUERY
            = "SELECT name FROM role WHERE id = ?";
    public static final String UPDATE_BY_ID_QUERY
            = "UPDATE role SET name = ? WHERE id= ?";
    public static final String DELETE_BY_ID_QUERY
            = "DELETE FROM role WHERE id=?";
    public static final String INSERT_QUERY
            = "INSERT  INTO role (name) VALUES (?)";
    public static final String FIND_BY_NAME
            = "SELECT id FROM role where name =?";

    private RoleQueries() {
    }
}

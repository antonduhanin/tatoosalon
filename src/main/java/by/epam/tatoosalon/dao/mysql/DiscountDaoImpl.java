package by.epam.tatoosalon.dao.mysql;

import by.epam.tatoosalon.dao.DiscountDao;
import by.epam.tatoosalon.entity.pojo.Discount;
import by.epam.tatoosalon.exception.DaoException;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static by.epam.tatoosalon.dao.mysql.constant.DiscountQueries.*;

public class DiscountDaoImpl extends BaseDao implements DiscountDao {

    @Override
    public Integer create(Discount entity) throws DaoException {
        try (PreparedStatement statement = connection.prepareStatement
                (INSERT_QUERY, Statement
                        .RETURN_GENERATED_KEYS)) {
            statement.setString(1, entity.getName());
            statement.setBigDecimal(2, entity.getValue());
            statement.executeUpdate();
            try (ResultSet resultSet = statement.getGeneratedKeys()) {
                if (resultSet.next()) {
                    return resultSet.getInt(1);
                } else {
                    throw new DaoException();
                }
            }
        } catch (SQLException e) {
            throw new DaoException();
        }
    }

    @Override
    public Discount read(int id) throws DaoException {
        try (PreparedStatement statement
                     = connection.prepareStatement(SELECT_BY_ID_QUERY)) {
            statement.setInt(1, id);
            Discount discount = null;
            try (ResultSet resultSet = statement.executeQuery()) {
                if (resultSet.next()) {
                    discount = new Discount();
                    discount.setId(id);
                    discount.setName(resultSet.getString("name"));
                    discount.setValue(resultSet.getBigDecimal("value"));
                }
            } catch (SQLException e) {
                throw new DaoException();
            }
            return discount;
        } catch (SQLException e) {
            throw new DaoException();
        }
    }

    @Override
    public void update(Discount entity) throws DaoException {
        try (PreparedStatement statement
                     = connection.prepareStatement(UPDATE_BY_ID_QUERY)) {
            statement.setString(1, entity.getName());
            statement.setBigDecimal(2, entity.getValue());
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new DaoException();
        }
    }

    @Override
    public void delete(int id) throws DaoException {
        try (PreparedStatement statement
                     = connection.prepareStatement(DELETE_BY_ID_QUERY)) {
            statement.setInt(1, id);
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new DaoException();
        }
    }

    @Override
    public List<Discount> getAll() throws DaoException {
        List<Discount> discountList = new ArrayList<>();
        try (PreparedStatement statement
                     = connection.prepareStatement(GET_ALL_DISCOUNTS)) {
            try (ResultSet resultSet = statement.executeQuery()) {
                if (resultSet.next()) {
                    Discount discount = new Discount();
                    discount.setId(resultSet.getInt("id"));
                    discount.setName(resultSet.getString("name"));
                    discount.setValue(resultSet.getBigDecimal("value"));
                    discountList.add(discount);
                }
            } catch (SQLException e) {
                throw new DaoException();
            }
            return discountList;
        } catch (SQLException e) {
            throw new DaoException();
        }
    }

    @Override
    public List<Discount> findDiscountsForUserId(int id) throws DaoException {
        return discontsByUserId(id, GET_DISCOUNTS_BY_USER);
    }

    @Override
    public void setDiscountForUserId(int idDiscount, int idUser, Date date) throws DaoException {
        try (PreparedStatement statement
                     = connection.prepareStatement(INSERT_DISCOUNT_FOR_USER)) {
            statement.setInt(1, idUser);
            statement.setInt(2, idDiscount);
            statement.setDate(3, new java.sql.Date(date.getTime()));
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new DaoException();
        }
    }

    @Override
    public List<Discount> findActiveDiscountsForUserId(int userId) throws
            DaoException {
        return discontsByUserId(userId, GET_ACTIVE_DISCOUNTS_FOR_USER);
    }

    @Override
    public void deleteDiscountForUserId(int userId) throws DaoException {
        try (PreparedStatement statement
                     = connection.prepareStatement(DELETE_DISCOUNT_FOR_USER)) {
            statement.setInt(1, userId);
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new DaoException();
        }
    }

    private List<Discount> discontsByUserId(int userId, String sql)
            throws DaoException {
        List<Discount> discountList = new ArrayList<>();
        try (PreparedStatement statement
                     = connection.prepareStatement(sql)) {
            statement.setInt(1, userId);
            try (ResultSet resultSet = statement.executeQuery()) {
                if (resultSet.next()) {
                    Discount discount = new Discount();
                    discount.setId(resultSet.getInt("discount.id"));
                    discount.setName(resultSet.getString("name"));
                    discount.setValue(resultSet.getBigDecimal("value"));
                    discount.setEndDiscount(resultSet.getDate("end_discount"));
                    discountList.add(discount);
                }
            } catch (SQLException e) {
                throw new DaoException();
            }
            return discountList;
        } catch (SQLException e) {
            throw new DaoException();
        }
    }

}

package by.epam.tatoosalon.dao.mysql;

import by.epam.tatoosalon.dao.UserDao;
import by.epam.tatoosalon.entity.pojo.User;
import by.epam.tatoosalon.exception.DaoException;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import static by.epam.tatoosalon.dao.mysql.constant.UserQueries.*;

public class UserDaoImpl extends BaseDao implements UserDao {

    public Integer create(User entity) throws DaoException {
        try (PreparedStatement statement
                     = connection.prepareStatement(INSERT_QUERY,
                Statement.RETURN_GENERATED_KEYS)) {
            statement.setString(1, entity.getLogin());
            statement.setString(2, entity.getPassword());
            statement.setInt(3, entity.getRoleId());
            statement.executeUpdate();
            try (ResultSet resultSet = statement.getGeneratedKeys()) {
                if (resultSet.next()) {
                    return resultSet.getInt(1);
                } else {
                    throw new DaoException();
                }
            }
        } catch (SQLException e) {
            throw new DaoException();
        }
    }

    public User read(int id) throws DaoException {
        try (PreparedStatement statement
                     = connection.prepareStatement(SELECT_BY_ID_QUERY)) {
            statement.setInt(1, id);
            User user = null;
            try (ResultSet resultSet = statement.executeQuery()) {
                if (resultSet.next()) {
                    user = new User();
                    user.setId(id);
                    user.setPassword(resultSet.getString("password"));
                    user.setLogin(resultSet.getString("login"));
                    user.setRoleId(resultSet.getInt("role_id"));
                }
            } catch (SQLException e) {
                throw new DaoException();
            }
            return user;
        } catch (SQLException e) {
            throw new DaoException();
        }
    }

    public void update(User entity) throws DaoException {
        try (PreparedStatement statement
                     = connection.prepareStatement(UPDATE_BY_ID_QUERY)) {
            statement.setString(1, entity.getLogin());
            statement.setString(2, entity.getPassword());
            statement.setInt(3, entity.getRoleId());
            statement.setInt(4, entity.getId());
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new DaoException();
        }
    }

    public void delete(int id) throws DaoException {
        try (PreparedStatement statement
                     = connection.prepareStatement(DELETE_BY_ID_QUERY)) {
            statement.setInt(1, id);
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new DaoException();
        }
    }

    @Override
    public List<User> getAll() throws DaoException {
        List<User> users = new ArrayList<>();
        try (PreparedStatement statement
                     = connection.prepareStatement(GET_ALL_USERS)) {
            try (ResultSet resultSet = statement.executeQuery()) {
                while (resultSet.next()) {
                    User user = new User();
                    user.setId(resultSet.getInt("id"));
                    user.setPassword(resultSet.getString("password"));
                    user.setLogin(resultSet.getString("login"));
                    user.setRoleId(resultSet.getInt("role_id"));
                    users.add(user);
                }
            } catch (SQLException e) {
                throw new DaoException();
            }
            return users;
        } catch (SQLException e) {
            throw new DaoException();
        }
    }

    @Override
    public User read(String login, String password) throws DaoException {
        try (PreparedStatement statement = connection
                .prepareStatement(SELECT_BY_LOGIN_AND_PASSWORD)) {
            statement.setString(1, login);
            statement.setString(2, password);
            User user = null;
            try (ResultSet resultSet = statement.executeQuery()) {
                if (resultSet.next()) {
                    user = new User();
                    user.setId(resultSet.getInt("id"));
                    user.setPassword(password);
                    user.setLogin(login);
                    user.setRoleId(resultSet.getInt("role_id"));
                }
            } catch (SQLException e) {
                throw new DaoException();
            }
            return user;
        } catch (SQLException e) {
            throw new DaoException();
        }
    }

    @Override
    public User readByLogin(String login) throws DaoException {
    try (PreparedStatement statement
                     = connection.prepareStatement(SELECT_BY_LOGIN)) {
            statement.setString(1, login);
            User user = null;
            try (ResultSet resultSet = statement.executeQuery()) {
                if (resultSet.next()) {
                    user = new User();
                    user.setId(resultSet.getInt("id"));
                    user.setLogin(login);
                    user.setRoleId(resultSet.getInt("role_id"));
                }
            } catch (SQLException e) {
                throw new DaoException();
            }
            return user;
        } catch (SQLException e) {
            throw new DaoException();
        }
    }
}

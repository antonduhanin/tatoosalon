package by.epam.tatoosalon.dao.mysql;

import by.epam.tatoosalon.dao.*;
import by.epam.tatoosalon.dao.pool.ConnectionPool;
import by.epam.tatoosalon.dao.pool.ConnectionPoolImpl;
import by.epam.tatoosalon.exception.DaoException;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class DaoManagerImpl implements DaoManager {
    private boolean isTransaction;
    private ConnectionPool connectionPool;
    private static Map<Class<? extends Dao<?>>, Class<? extends BaseDao>> classMap = new ConcurrentHashMap<>();

    static {
        classMap.put(UserDao.class, UserDaoImpl.class);
        classMap.put(TattooDao.class, TattooDaoImpl.class);
        classMap.put(RoleDao.class, RoleDaoImpl.class);
        classMap.put(ImageDao.class, ImageDaoImpl.class);
        classMap.put(DiscountDao.class, DiscountDaoImpl.class);
        classMap.put(OrderDao.class, OrderDaoImpl.class);
    }

    private Connection connection;


    @SuppressWarnings("unchecked")
    @Override
    public <Type extends Dao<?>> Type createDao(Class<Type> key) throws DaoException {
        Class<? extends BaseDao> value = classMap.get(key);
        if (value != null) {
            try {
                BaseDao dao = value.newInstance();
                dao.setConnection(connection);
                return (Type) dao;
            } catch (InstantiationException | IllegalAccessException e) {
                throw new DaoException(e.getMessage());
            }
        } else {
            throw new DaoException("impl of dao not found");
        }
    }

    @Override
    public void commit() throws DaoException {
        try {
            connection.commit();
        } catch (SQLException e) {
            throw new DaoException(e.getMessage());
        }
    }

    @Override
    public void rollback() throws DaoException {
        try {
            connection.rollback();
        } catch (SQLException e) {
            throw new DaoException(e.getMessage());
        }
    }

    @Override
    public void closeConnection() throws DaoException {
       connectionPool.checkIn(connection);
    }

    @Override
    public boolean isTransaction() {
        return isTransaction;
    }

    @Override
    public void setTransaction(boolean transaction) throws DaoException {
        isTransaction = transaction;
        if (isTransaction) {
            try {
                connection.setAutoCommit(false);
            } catch (SQLException e) {
                throw new DaoException("can't not turn off auto transactions: "
                        + e.getMessage());
            }
        }
    }

    public void openConnection() throws DaoException {
        connectionPool = ConnectionPoolImpl.getInstance();
        connection = connectionPool.checkOut();
    }

}

package by.epam.tatoosalon.dao.mysql;

import by.epam.tatoosalon.dao.RoleDao;
import by.epam.tatoosalon.entity.pojo.Role;
import by.epam.tatoosalon.exception.DaoException;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import static by.epam.tatoosalon.dao.mysql.constant.RoleQueries.*;

public class RoleDaoImpl extends BaseDao implements RoleDao {
    @Override
    public Integer create(Role entity) throws DaoException {
        try (PreparedStatement statement
                     = connection.prepareStatement(INSERT_QUERY,
                Statement.RETURN_GENERATED_KEYS)) {
            statement.setString(1, entity.getName());
            statement.executeUpdate();
            try (ResultSet resultSet = statement.getGeneratedKeys()) {
                if (resultSet.next()) {
                    return resultSet.getInt(1);
                } else {
                    throw new DaoException();
                }
            }
        } catch (SQLException e) {
            throw new DaoException();
        }
    }

    @Override
    public Role read(int id) throws DaoException {
        try (PreparedStatement statement
                     = connection.prepareStatement(SELECT_BY_ID_QUERY)) {
            statement.setInt(1, id);
            Role role = null;
            try (ResultSet resultSet = statement.executeQuery()) {
                if (resultSet.next()) {
                    role = new Role();
                    role.setId(id);
                    role.setName(resultSet.getString("name"));
                }
            } catch (SQLException e) {
                throw new DaoException();
            }
            return role;
        } catch (SQLException e) {
            throw new DaoException();
        }
    }

    @Override
    public void update(Role entity) throws DaoException {
        try (PreparedStatement statement
                     = connection.prepareStatement(UPDATE_BY_ID_QUERY)) {
            statement.setString(1, entity.getName());
            statement.setInt(2, entity.getId());
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new DaoException();
        }
    }

    @Override
    public void delete(int id) throws DaoException {
        try (PreparedStatement statement
                     = connection.prepareStatement(DELETE_BY_ID_QUERY)) {
            statement.setInt(1, id);
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new DaoException();
        }
    }

    @Override
    public List<Role> getAll() throws DaoException {
        List<Role> roleList = new ArrayList<>();
        try (PreparedStatement statement
                     = connection.prepareStatement(GET_ALL_ROLES)) {
            try (ResultSet resultSet = statement.executeQuery()) {
                if (resultSet.next()) {
                    Role role = new Role();
                    role.setId(resultSet.getInt("id"));
                    role.setName(resultSet.getString("name"));
                    roleList.add(role);
                }
            } catch (SQLException e) {
                throw new DaoException();
            }
            return roleList;
        } catch (SQLException e) {
            throw new DaoException();
        }
    }

    @Override
    public Role readByName(String name) throws DaoException {
        try (PreparedStatement statement
                     = connection.prepareStatement(FIND_BY_NAME)) {
            statement.setString(1, name);
            Role role = null;
            try (ResultSet resultSet = statement.executeQuery()) {
                if (resultSet.next()) {
                    role = new Role();
                    role.setId(resultSet.getInt("id"));
                    role.setName(name);
                }
            } catch (SQLException e) {
                throw new DaoException();
            }
            return role;
        } catch (SQLException e) {
            throw new DaoException();
        }
    }
}

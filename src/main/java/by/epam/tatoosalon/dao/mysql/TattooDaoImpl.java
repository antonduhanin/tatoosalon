package by.epam.tatoosalon.dao.mysql;

import by.epam.tatoosalon.dao.TattooDao;
import by.epam.tatoosalon.entity.pojo.Rating;
import by.epam.tatoosalon.entity.pojo.Tattoo;
import by.epam.tatoosalon.exception.DaoException;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import static by.epam.tatoosalon.dao.mysql.constant.TattooQueries.*;

public class TattooDaoImpl extends BaseDao implements TattooDao {
    @Override
    public Integer create(Tattoo entity) throws DaoException {
        try (PreparedStatement statement
                     = connection.prepareStatement(INSERT_QUERY,
                Statement.RETURN_GENERATED_KEYS)) {
            statement.setString(1, entity.getName());
            statement.setBigDecimal(2, entity.getPrice());
            statement.setString(3, entity.getState());
            statement.setInt(4, entity.getUserId());
            statement.executeUpdate();
            try (ResultSet resultSet = statement.getGeneratedKeys()) {
                if (resultSet.next()) {
                    return resultSet.getInt(1);
                } else {
                    throw new DaoException();
                }
            }
        } catch (SQLException e) {
            throw new DaoException();
        }
    }

    @Override
    public Tattoo read(int id) throws DaoException {
        try (PreparedStatement statement
                     = connection.prepareStatement(SELECT_BY_ID_QUERY)) {
            statement.setInt(1, id);
            Tattoo tattoo = null;
            try (ResultSet resultSet = statement.executeQuery()) {
                if (resultSet.next()) {
                    tattoo = new Tattoo();
                    tattoo.setId(id);
                    tattoo.setName(resultSet.getString("name"));
                    tattoo.setPrice(resultSet.getBigDecimal("price"));
                    tattoo.setState(resultSet.getString("state"));
                    tattoo.setUserId(resultSet.getInt("user_id"));
                    tattoo.setRatingList(getRatingListByTatooId(id));
                }
            } catch (SQLException e) {
                throw new DaoException();
            }
            return tattoo;
        } catch (SQLException e) {
            throw new DaoException();
        }
    }

    @Override
    public void update(Tattoo entity) throws DaoException {
        try (PreparedStatement statement
                     = connection.prepareStatement(UPDATE_BY_ID_QUERY)) {
            statement.setString(1, entity.getName());
            statement.setBigDecimal(2, entity.getPrice());
            statement.setString(3, entity.getState());
            statement.setInt(4, entity.getId());
            statement.setInt(5, entity.getId());
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new DaoException();
        }
    }

    @Override
    public void delete(int id) throws DaoException {
        try (PreparedStatement statement
                     = connection.prepareStatement(DELETE_BY_ID_QUERY)) {
            statement.setInt(1, id);
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new DaoException();
        }
    }

    @Override
    public List<Tattoo> getAll() throws DaoException {
        List<Tattoo> tatooList = new ArrayList<>();
        try (PreparedStatement statement
                     = connection.prepareStatement(GET_ALL_TATOO)) {
            try (ResultSet resultSet = statement.executeQuery()) {
                while (resultSet.next()) {
                    Tattoo tattoo = new Tattoo();
                    tattoo.setId(resultSet.getInt("id"));
                    tattoo.setName(resultSet.getString("name"));
                    tattoo.setPrice(resultSet
                            .getBigDecimal("price"));
                    tattoo.setState(resultSet.getString("state"));
                    tattoo.setUserId(resultSet.getInt("user_id"));
                    tattoo.setRatingList(getRatingListByTatooId(tattoo.getId()));
                    tatooList.add(tattoo);
                }
            } catch (SQLException e) {
                throw new DaoException();
            }
            return tatooList;
        } catch (SQLException e) {
            throw new DaoException();
        }
    }

    @Override
    public void rateWork(int tatooId, int orderId, int rateValue)
            throws DaoException {
        rate(tatooId, orderId, rateValue, INSERT_RATE_WORK);
    }

    @Override
    public void rateTattoo(int tatooId, int userId, int rateValue)
            throws DaoException {
        rate(tatooId, userId, rateValue, INSERT_RATE_TATOO);
    }

    @Override
    public void deleteRatingTattoo(int tattooId, int userId) throws
            DaoException {
        deleteRating(tattooId, userId, DELETE_RATING_TATOO);
    }

    @Override
    public void deleteRatingWork(int tattooId, int orderId) throws
            DaoException {
        deleteRating(tattooId, orderId, DELETE_RATING_WORK);
    }

    private List<Rating> getRatingListByTatooId(int tattooId)
            throws DaoException {
        List<Rating> ratingList = new ArrayList<>();
        try (PreparedStatement statement
                     = connection.prepareStatement(GET_RATING_BY_TATOO)) {
            statement.setInt(1, tattooId);
            try (ResultSet resultSet = statement.executeQuery()) {
                if (resultSet.next()) {
                    Rating rating = new Rating();
                    rating.setTatooId(tattooId);
                    rating.setUserId(resultSet.getInt("user_id"));
                    rating.setValue(resultSet.getInt("value"));
                    ratingList.add(rating);
                }
            } catch (SQLException e) {
                throw new DaoException();
            }
            return ratingList;
        } catch (SQLException e) {
            throw new DaoException();
        }
    }

    private void deleteRating(int tattooId, int entityId, String sql) throws
            DaoException {
        try (PreparedStatement statement
                     = connection.prepareStatement(sql)) {
            statement.setInt(1, tattooId);
            statement.setInt(2, entityId);
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new DaoException();
        }
    }

    private void rate(int tattooId, int enityId, int rateValue, String sql)
            throws DaoException {
        try (CallableStatement statement
                     = connection.prepareCall(sql)) {
            statement.setInt(1, tattooId);
            statement.setInt(2, enityId);
            statement.setInt(3, rateValue);
            statement.execute();
        } catch (SQLException e) {
            throw new DaoException();
        }
    }

}

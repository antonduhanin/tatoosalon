package by.epam.tatoosalon.dao.mysql.constant;

public class OrderQueries {
    public static final String INSERT_QUERY
            = "INSERT INTO `order` (user_id, total_price, `date`, is_paid)"
            + " VALUES (?,?,?,?)";
    public static final String SELECT_BY_ID_QUERY
            = "SELECT user_id,total_price,date,is_paid FROM `order`"
            + " WHERE id = ?";
    public static final String UPDATE_BY_ID_QUERY
            = "UPDATE `order` SET user_id=?, total_price = ?, `date` = ?, "
            + "is_paid = ? WHERE id= ?";
    public static final String DELETE_BY_ID_QUERY
            = "DELETE FROM `order` WHERE id=?";
    public static final String GET_ALL_ORDERS
            = "SELECT id, user_id, total_price, `date`, is_paid FROM `order`";
    public static final String GET_ORDERS_BY_USER
            = "SELECT id, user_id, total_price, `date`, is_paid FROM `order`"
            + "where user_id = ?";
    public static final String ADD_TATOO_TO_ORDER
            = "INSERT INTO order_tatoo "
            + "(order_id, tatoo_id, name_master) VALUES(?,?,?)";
    public static final String GET_WORK_INFO_BY_ORDER
            ="SELECT order_id, tatoo_id, rating_work, name_master"
            + " FROM order_tatoo WHERE order_id = ?";
    public static final String DELETE_TATTOO_FROM_ORDER
            = "DELETE FROM ORDER_TATOO WHERE order_id=? and tatoo_id =? ";

    private OrderQueries() {
    }
}

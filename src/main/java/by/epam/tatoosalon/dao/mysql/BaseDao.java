package by.epam.tatoosalon.dao.mysql;

import java.sql.Connection;

abstract class BaseDao  {
    protected Connection connection;

    public BaseDao() {
    }

    public BaseDao(Connection connection) {
        this.connection = connection;
    }

    public void setConnection(Connection connection) {
        this.connection = connection;
    }
}

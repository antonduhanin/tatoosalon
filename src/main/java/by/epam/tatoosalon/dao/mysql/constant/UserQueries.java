package by.epam.tatoosalon.dao.mysql.constant;

public class UserQueries {
    public static final String INSERT_QUERY
            = "INSERT INTO user (login, password, role_id) VALUES (?,?,?)";
    public static final String SELECT_BY_ID_QUERY
            = "SELECT id, login, password, role_id FROM user WHERE id = ?";
    public static final String UPDATE_BY_ID_QUERY
            = "UPDATE user SET login=?, password = ?, role_id = ? WHERE id= ?";
    public static final String DELETE_BY_ID_QUERY
            = "DELETE FROM USER WHERE id=?";
    public static final String GET_ALL_USERS
            = "SELECT id, password, login, role_id FROM user";
    public static final String SELECT_BY_LOGIN_AND_PASSWORD
            = "SELECT id, login, password, role_id FROM user WHERE login = ? "
            + "and password = ?";
    public static final String SELECT_BY_LOGIN
            = "SELECT id, login, password, role_id FROM user WHERE login = ? ";


    private UserQueries() {
    }
}

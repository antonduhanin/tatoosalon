package by.epam.tatoosalon.dao.mysql.constant;

public class DiscountQueries {
    public static final String INSERT_QUERY
            = "INSERT INTO `discount` (name, value) VALUES (?,?,?)";
    public static final String SELECT_BY_ID_QUERY
            = "SELECT name, value FROM `discount` WHERE id = ? ";
    public static final String UPDATE_BY_ID_QUERY
            = "UPDATE `discount` SET name = ?, value = ? WHERE id= ?";
    public static final String DELETE_BY_ID_QUERY
            = "DELETE FROM discount WHERE id=?";
    public static final String GET_ALL_DISCOUNTS
            = "SELECT id, name, value FROM discount";
    public static final String GET_DISCOUNTS_BY_USER
            = "SELECT discount.id, name, value, end_discount FROM discount"
            + " inner join user_discount"
            + " on user_discount.discount_id = discount.id"
            + " where user_discount.user_id  = ? ";
    public static final String GET_ACTIVE_DISCOUNTS_FOR_USER
            = "SELECT discount.id, name, value, end_discount FROM discount"
            + " inner join user_discount"
            + " on user_discount.discount_id = discount.id"
            + " where user_discount.user_id  = ? and end_discount > NOW()";
    public static final String INSERT_DISCOUNT_FOR_USER
            = "INSERT INTO `user_discount` (id_user, id_discount, end_discount)"
            + " VALUES (?,?,?)";
    public static final String DELETE_DISCOUNT_FOR_USER
            = "DELETE FROM user_discount WHERE user_id=?";

    private DiscountQueries() {
    }
}

package by.epam.tatoosalon.dao.mysql;

import by.epam.tatoosalon.dao.OrderDao;
import by.epam.tatoosalon.entity.pojo.Order;
import by.epam.tatoosalon.entity.pojo.WorkInfo;
import by.epam.tatoosalon.exception.DaoException;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import static by.epam.tatoosalon.dao.mysql.constant.OrderQueries.*;

public class OrderDaoImpl extends BaseDao implements OrderDao {
    @Override
    public Integer create(Order entity) throws DaoException {
        try (PreparedStatement statement = connection.prepareStatement
                (INSERT_QUERY, Statement.RETURN_GENERATED_KEYS)) {
            statement.setInt(1, entity.getUserId());
            statement.setBigDecimal(2, entity.getTotalPrice());
            statement.setDate(3, new java.sql.Date(entity.getDate().getTime()));
            statement.setBoolean(4, entity.isPaid());
            statement.executeUpdate();
            try (ResultSet resultSet = statement.getGeneratedKeys()) {
                if (resultSet.next()) {
                    return resultSet.getInt(1);
                } else {
                    throw new DaoException();
                }
            }
        } catch (SQLException e) {
            throw new DaoException();
        }
    }

    @Override
    public Order read(int id) throws DaoException {
        try (PreparedStatement statement
                     = connection.prepareStatement(SELECT_BY_ID_QUERY)) {
            statement.setInt(1, id);
            Order order = null;
            try (ResultSet resultSet = statement.executeQuery()) {
                if (resultSet.next()) {
                    order = new Order();
                    order.setId(id);
                    order.setUserId(resultSet.getInt("user_id"));
                    order.setTotalPrice(resultSet.getBigDecimal("total_price"));
                    order.setDate(resultSet.getDate("date"));
                    order.setPaid(resultSet.getBoolean("is_paid"));
                    order.setWorkInfo(getWorkInfoByOrder(order.getId()));
                }
            } catch (SQLException e) {
                throw new DaoException();
            }
            return order;
        } catch (SQLException e) {
            throw new DaoException();
        }
    }

    @Override
    public void update(Order entity) throws DaoException {
        try (PreparedStatement statement
                     = connection.prepareStatement(UPDATE_BY_ID_QUERY)) {
            statement.setInt(1, entity.getUserId());
            statement.setBigDecimal(2, entity.getTotalPrice());
            statement.setDate(3, new java.sql.Date(entity.getDate().getTime()));
            statement.setBoolean(4, entity.isPaid());
            statement.setInt(5, entity.getId());
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new DaoException();
        }
    }

    @Override
    public void delete(int id) throws DaoException {
        try (PreparedStatement statement
                     = connection.prepareStatement(DELETE_BY_ID_QUERY)) {
            statement.setInt(1, id);
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new DaoException();
        }
    }

    @Override
    public List<Order> getAll() throws DaoException {
        List<Order> orderList = new ArrayList<>();
        try (PreparedStatement statement
                     = connection.prepareStatement(GET_ALL_ORDERS)) {
            try (ResultSet resultSet = statement.executeQuery()) {
                if (resultSet.next()) {
                    Order order = new Order();
                    order.setId(resultSet.getInt("id"));
                    order.setUserId(resultSet.getInt("user_id"));
                    order.setTotalPrice(resultSet.getBigDecimal("total_price"));
                    order.setDate(resultSet.getDate("date"));
                    order.setPaid(resultSet.getBoolean("isPaid"));
                    order.setWorkInfo(getWorkInfoByOrder(order.getId()));
                    orderList.add(order);
                }
            } catch (SQLException e) {
                throw new DaoException();
            }
            return orderList;
        } catch (SQLException e) {
            throw new DaoException();
        }

    }

    @Override
    public List<Order> findOrdersByUser(int userId) throws DaoException {
        List<Order> orderList = new ArrayList<>();
        try (PreparedStatement statement
                     = connection.prepareStatement(GET_ORDERS_BY_USER)) {
            statement.setInt(1, userId);
            try (ResultSet resultSet = statement.executeQuery()) {
                if (resultSet.next()) {
                    Order order = new Order();
                    order.setId(resultSet.getInt("id"));
                    order.setUserId(userId);
                    order.setTotalPrice(resultSet.getBigDecimal("total_price"));
                    order.setDate(resultSet.getDate("date"));
                    order.setPaid(resultSet.getBoolean("is_paid"));
                    orderList.add(order);
                }
            } catch (SQLException e) {
                throw new DaoException();
            }
            return orderList;
        } catch (SQLException e) {
            throw new DaoException();
        }
    }

    @Override
    public void addTattooInOrder(WorkInfo workInfo)
            throws DaoException {
        try (PreparedStatement statement = connection.prepareStatement
                (ADD_TATOO_TO_ORDER)) {
            statement.setInt(1, workInfo.getOrderId());
            statement.setInt(2, workInfo.getTattooId());
            statement.setString(3, workInfo.getNameMaster());
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new DaoException();
        }
    }

    @Override
    public void deleteTattooInOrder(WorkInfo workInfo) throws DaoException {
        try (PreparedStatement statement
                     = connection.prepareStatement(DELETE_BY_ID_QUERY)) {
            statement.setInt(1, workInfo.getOrderId());
            statement.setInt(2, workInfo.getTattooId());
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new DaoException();
        }
    }


    private List<WorkInfo> getWorkInfoByOrder(int orderId) throws DaoException {
        List<WorkInfo> workInfoList = new ArrayList<>();
        try (PreparedStatement statement
                     = connection.prepareStatement(GET_WORK_INFO_BY_ORDER)) {
            statement.setInt(1, orderId);
            try (ResultSet resultSet = statement.executeQuery()) {
                if (resultSet.next()) {
                    WorkInfo workInfo = new WorkInfo();
                    workInfo.setOrderId(orderId);
                    workInfo.setTattooId(resultSet.getInt("tatoo_id"));
                    workInfo.setRatingWork(resultSet.getInt("rating_work"));
                    workInfo.setNameMaster(resultSet.getString("name_master"));
                    workInfoList.add(workInfo);
                }
            } catch (SQLException e) {
                throw new DaoException();
            }
            return workInfoList;
        } catch (SQLException e) {
            throw new DaoException();
        }
    }

}

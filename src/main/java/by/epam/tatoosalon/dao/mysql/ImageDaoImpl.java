package by.epam.tatoosalon.dao.mysql;

import by.epam.tatoosalon.dao.ImageDao;
import by.epam.tatoosalon.entity.pojo.Image;
import by.epam.tatoosalon.exception.DaoException;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import static by.epam.tatoosalon.dao.mysql.constant.ImageQueries.*;

public class ImageDaoImpl extends BaseDao implements ImageDao {

    @Override
    public Integer create(Image entity) throws DaoException {
        try (PreparedStatement statement = connection.prepareStatement
                (INSERT_QUERY, Statement
                        .RETURN_GENERATED_KEYS)) {
            statement.setString(1, entity.getPath());
            statement.setInt(2, entity.getTatooId());
            statement.executeUpdate();
            try (ResultSet resultSet = statement.getGeneratedKeys()) {
                if (resultSet.next()) {
                    return resultSet.getInt(1);
                } else {
                    throw new DaoException();
                }
            }
        } catch (SQLException e) {
            throw new DaoException();
        }
    }

    @Override
    public Image read(int id) throws DaoException {
        try (PreparedStatement statement
                     = connection.prepareStatement(SELECT_BY_ID_QUERY)) {
            statement.setInt(1, id);
            Image image = null;
            try (ResultSet resultSet = statement.executeQuery()) {
                if (resultSet.next()) {
                    image = new Image();
                    image.setId(id);
                    image.setPath(resultSet.getString("path"));
                    image.setTatooId(resultSet.getInt("tatoo_id"));
                }
            } catch (SQLException e) {
                throw new DaoException();
            }
            return image;
        } catch (SQLException e) {
            throw new DaoException();
        }
    }

    @Override
    public void update(Image entity) throws DaoException {
        try (PreparedStatement statement
                     = connection.prepareStatement(UPDATE_BY_ID_QUERY)) {
            statement.setString(1, entity.getPath());
            statement.setInt(2, entity.getTatooId());
            statement.setInt(3, entity.getId());
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new DaoException();
        }
    }

    @Override
    public void delete(int id) throws DaoException {
        try (PreparedStatement statement
                     = connection.prepareStatement(DELETE_BY_ID_QUERY)) {
            statement.setInt(1, id);
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new DaoException();
        }
    }

    @Override
    public List<Image> getAll() throws DaoException {
        List<Image> imageList = new ArrayList<>();
        try (PreparedStatement statement
                     = connection.prepareStatement(GET_ALL_IMAGES)) {
            try (ResultSet resultSet = statement.executeQuery()) {
                while (resultSet.next()) {
                    Image image = new Image();
                    image.setId(resultSet.getInt("id"));
                    image.setPath(resultSet.getString("path"));
                    image.setTatooId(resultSet.getInt("tatoo_id"));
                    imageList.add(image);
                }
            } catch (SQLException e) {
                throw new DaoException();
            }
            return imageList;
        } catch (SQLException e) {
            throw new DaoException();
        }
    }

    @Override
    public List<Image> getImageByTattoo(int tattooId) throws DaoException {
        List<Image> imageList = new ArrayList<>();
        try (PreparedStatement statement
                     = connection.prepareStatement(GET_ALL_IMAGES_BY_TATTOO)) {
            statement.setInt(1, tattooId);
            try (ResultSet resultSet = statement.executeQuery()) {
                while (resultSet.next()) {
                    Image image = new Image();
                    image.setId(resultSet.getInt("id"));
                    image.setPath(resultSet.getString("path"));
                    image.setTatooId(tattooId);
                    imageList.add(image);
                }
            } catch (SQLException e) {
                throw new DaoException();
            }
            return imageList;
        } catch (SQLException e) {
            throw new DaoException();
        }
    }
}

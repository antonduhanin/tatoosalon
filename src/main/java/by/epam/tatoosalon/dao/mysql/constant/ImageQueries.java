package by.epam.tatoosalon.dao.mysql.constant;

public class ImageQueries {
    public static final String INSERT_QUERY
            = "INSERT INTO `image` (path, tatoo_id) VALUES (?,?)";
    public static final String SELECT_BY_ID_QUERY
            = "SELECT path, tatoo_id FROM `image` WHERE id = ?";
    public static final String UPDATE_BY_ID_QUERY
            = "UPDATE `image` SET path = ?, tatoo_id = ? WHERE id= ?";
    public static final String DELETE_BY_ID_QUERY
            = "DELETE FROM `image` WHERE id=?";
    public static final String GET_ALL_IMAGES
            = "SELECT id, path, tatoo_id FROM image";
    public static final String GET_ALL_IMAGES_BY_TATTOO
            = "SELECT id, path FROM image WHERE tatoo_id=?";
    private ImageQueries() {
    }
}

package by.epam.tatoosalon.dao.mysql.constant;

public class TattooQueries {
    public static final String INSERT_QUERY
            = "INSERT INTO tatoo (name, price, state, user_id)"
            + " VALUES (?,?,?,?)";
    public static final String SELECT_BY_ID_QUERY
            = "SELECT name, price, state, user_id FROM tatoo WHERE id = ?";
    public static final String UPDATE_BY_ID_QUERY
            = "UPDATE tatoo SET name=?, price = ?, state = ?, user_id = ?"
            + " WHERE id= ?";
    public static final String DELETE_BY_ID_QUERY
            = "DELETE FROM tatoo WHERE id=?";
    public static final String GET_ALL_TATOO
            = "SELECT id, name, price, state, user_id FROM tatoo";
    public static final String INSERT_RATE_WORK
            = "call rateWork(?,?,?)";
    public static final String INSERT_RATE_TATOO
            = "call rateTatoo(?,?,?)";
    public static final String GET_RATING_BY_TATOO
            = "SELECT tatoo_id, user_id, `value` from rating WHERE tatoo_id = ?";
    public static final String DELETE_RATING_TATOO
            = "DELETE FROM rating WHERE tatoo_id = ? and user_id = ?";
    public static final String DELETE_RATING_WORK
            = "DELETE FROM order_tatoo WHERE tatoo_id = ? and order_id = ?";

    private TattooQueries() {
    }
}

package by.epam.tatoosalon.entity.builder.impl;

import by.epam.tatoosalon.entity.builder.UserBuilder;
import by.epam.tatoosalon.entity.dto.UserDto;
import by.epam.tatoosalon.entity.pojo.Role;
import by.epam.tatoosalon.entity.pojo.User;
import by.epam.tatoosalon.exception.ServiceException;
import by.epam.tatoosalon.service.RoleService;

public class UserBuilderImpl implements UserBuilder {
    private int id;
    private String login;
    private String password;
    private int roleId;
    private Role role;

    public UserBuilderImpl() {
    }

    @Override
    public void setLogin(String login) {
        this.login = login;
    }

    @Override
    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public void setId(int id) {
        this.id = id;
    }

    @Override
    public void setRoleId(int roleId) {
        this.roleId = roleId;
    }

    @Override
    public void setRole(Role role) {
        this.role = role;
    }

    @Override
    public UserDto buildClient(RoleService roleService,
                               String nameOfRole) throws ServiceException {
        role = roleService.findByName(nameOfRole);
        return new UserDto(id, login, password, role);
    }

    @Override
    public User buildUser() {
        return new User(id, login, password, roleId);
    }

    @Override
    public UserDto buildUserDto() {
        return new UserDto(id, login, password, role);
    }
}

package by.epam.tatoosalon.entity.builder;

import by.epam.tatoosalon.entity.dto.TattooDto;
import by.epam.tatoosalon.entity.pojo.Image;
import by.epam.tatoosalon.entity.pojo.Rating;
import by.epam.tatoosalon.entity.pojo.Tattoo;

import java.math.BigDecimal;
import java.util.IntSummaryStatistics;
import java.util.List;

public interface TattoBuilder {
    Tattoo buildTattoo();

    TattooDto buildTattooDto();

    void setId(int id);

    void setName(String name);

    void setPrice(BigDecimal price);

    void setState(String state);

    void setUserId(int userId);

    void setRatingList(List<Rating> ratingList);

    void setRatingStatistics(IntSummaryStatistics ratingStatistics);

    void setImageList(List<Image> imageList);
}

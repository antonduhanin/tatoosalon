package by.epam.tatoosalon.entity.builder;

import by.epam.tatoosalon.entity.dto.UserDto;
import by.epam.tatoosalon.entity.pojo.Role;
import by.epam.tatoosalon.entity.pojo.User;
import by.epam.tatoosalon.exception.ServiceException;
import by.epam.tatoosalon.service.RoleService;

public interface UserBuilder {
    User buildUser();

    UserDto buildUserDto();

    UserDto buildClient(RoleService roleService,
                        String nameOfRole) throws ServiceException;

    void setLogin(String login);

    void setPassword(String password);

    void setId(int id);

    void setRoleId(int roleId);

    void setRole(Role role);
}

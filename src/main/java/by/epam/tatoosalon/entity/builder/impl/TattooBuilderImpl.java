package by.epam.tatoosalon.entity.builder.impl;

import by.epam.tatoosalon.entity.builder.TattoBuilder;
import by.epam.tatoosalon.entity.dto.TattooDto;
import by.epam.tatoosalon.entity.pojo.Image;
import by.epam.tatoosalon.entity.pojo.Rating;
import by.epam.tatoosalon.entity.pojo.Tattoo;

import java.math.BigDecimal;
import java.util.IntSummaryStatistics;
import java.util.List;

public class TattooBuilderImpl implements TattoBuilder {
    private int id;
    private String name;
    private BigDecimal price;
    private String state;
    private int userId;
    private List<Rating> ratingList;
    private IntSummaryStatistics ratingStatistics;
    private List<Image> imageList;

    @Override
    public void setId(int id) {
        this.id = id;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    @Override
    public void setState(String state) {
        this.state = state;
    }

    @Override
    public void setUserId(int userId) {
        this.userId = userId;
    }

    @Override
    public void setRatingList(List<Rating> ratingList) {
        this.ratingList = ratingList;
    }

    @Override
    public void setRatingStatistics(IntSummaryStatistics ratingStatistics) {
        this.ratingStatistics = ratingStatistics;
    }

    @Override
    public void setImageList(List<Image> imageList) {
        this.imageList = imageList;
    }

    @Override
    public Tattoo buildTattoo() {
        return new Tattoo(id, name, price, state, userId, ratingList);
    }

    @Override
    public TattooDto buildTattooDto() {
        TattooDto tattooDto = new TattooDto();
        tattooDto.setId(id);
        tattooDto.setImageList(imageList);
        tattooDto.setName(name);
        tattooDto.setUserId(userId);
        tattooDto.setPrice(price);
        tattooDto.setRatingList(ratingList);
        tattooDto.setRatingStatistics(ratingStatistics);
        tattooDto.setImageList(imageList);
        tattooDto.setState(state);
        return tattooDto;
    }

}

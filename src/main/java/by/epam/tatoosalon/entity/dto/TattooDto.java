package by.epam.tatoosalon.entity.dto;

import by.epam.tatoosalon.entity.BaseObj;
import by.epam.tatoosalon.entity.pojo.Image;
import by.epam.tatoosalon.entity.pojo.Rating;

import java.math.BigDecimal;
import java.util.IntSummaryStatistics;
import java.util.List;

public class TattooDto extends BaseObj {
    private String name;
    private BigDecimal price;
    private String state;
    private int userId;
    private List<Rating> ratingList;
    private IntSummaryStatistics ratingStatistics;
    private List<Image> imageList;

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof TattooDto)) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }

        TattooDto tattooDto = (TattooDto) o;

        if (userId != tattooDto.userId) {
            return false;
        }
        if (name != null ? !name.equals(tattooDto.name)
                : tattooDto.name != null) {
            return false;
        }
        if (price != null
                ? !(price.compareTo(tattooDto.price) == 0)
                : tattooDto.price != null) {
            return false;
        }
        if (state != null ? !state.equals(tattooDto.state)
                : tattooDto.state != null) {
            return false;
        }
        if (ratingList != null ? !ratingList.equals(tattooDto.ratingList)
                : tattooDto.ratingList != null) {
            return false;
        }
        if (ratingStatistics != null
                ? !ratingStatistics.equals(tattooDto.ratingStatistics)
                : tattooDto.ratingStatistics != null) {
            return false;
        }
        return imageList != null ? imageList.equals(tattooDto.imageList)
                : tattooDto.imageList == null;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (price != null ? price.hashCode() : 0);
        result = 31 * result + (state != null ? state.hashCode() : 0);
        result = 31 * result + userId;
        result = 31 * result + (ratingList != null
                ? ratingList.hashCode() : 0);
        result = 31 * result + (ratingStatistics != null
                ? ratingStatistics.hashCode() : 0);
        result = 31 * result + (imageList != null
                ? imageList.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "TattooDto{" +
                "name='" + name + '\'' +
                ", price=" + price +
                ", state='" + state + '\'' +
                ", userId=" + userId +
                ", ratingList=" + ratingList +
                ", ratingStatistics=" + ratingStatistics +
                ", imageList=" + imageList +
                "} " + super.toString();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public List<Rating> getRatingList() {
        return ratingList;
    }

    public void setRatingList(List<Rating> ratingList) {
        this.ratingList = ratingList;
    }

    public IntSummaryStatistics getRatingStatistics() {
        return ratingStatistics;
    }

    public void setRatingStatistics(IntSummaryStatistics ratingStatistics) {
        this.ratingStatistics = ratingStatistics;
    }

    public List<Image> getImageList() {
        return imageList;
    }

    public void setImageList(List<Image> imageList) {
        this.imageList = imageList;
    }

}

package by.epam.tatoosalon.entity.dto;

import by.epam.tatoosalon.entity.BaseObj;
import by.epam.tatoosalon.entity.pojo.Role;

public class UserDto extends BaseObj {
    private String login;
    private String password;
    private Role role;

    public UserDto() {
    }

    public UserDto(int id, String login, String password, Role role) {
        super(id);
        this.login = login;
        this.password = password;
        this.role = role;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof UserDto)) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }

        UserDto userDTO = (UserDto) o;

        if (login != null ? !login.equals(userDTO.login)
                : userDTO.login != null) {
            return false;
        }
        if (password != null
                ? !password.equals(userDTO.password)
                : userDTO.password != null) {
            return false;
        }
        return role != null ? role.equals(userDTO.role) : userDTO.role == null;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (login != null ? login.hashCode() : 0);
        result = 31 * result + (password != null ? password.hashCode() : 0);
        result = 31 * result + (role != null ? role.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "UserDTO{" +
                "login='" + login + '\'' +
                ", password='" + password + '\'' +
                ", role=" + role +
                "} " + super.toString();
    }
}

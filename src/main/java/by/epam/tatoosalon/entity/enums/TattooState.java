package by.epam.tatoosalon.entity.enums;

public enum TattooState {
    PUBLISHED("published"),
    APPROVED("approved"),
    WAIT("wait");
    private String state;

    TattooState(String state) {
        this.state = state;
    }

    public String getState() {
        return state;
    }
}

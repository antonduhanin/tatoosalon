package by.epam.tatoosalon.entity;

public class BaseObj {
    private int id;

    public BaseObj() {
    }

    public BaseObj(int id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof BaseObj)) {
            return false;
        }

        BaseObj baseObj = (BaseObj) o;

        return id == baseObj.id;
    }

    @Override
    public int hashCode() {
        return id;
    }

    @Override
    public String toString() {
        return "BaseObj{" +
                "id=" + id +
                '}';
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}

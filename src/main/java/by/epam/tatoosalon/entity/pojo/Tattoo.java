package by.epam.tatoosalon.entity.pojo;

import by.epam.tatoosalon.entity.BaseObj;

import java.math.BigDecimal;
import java.util.List;

public class Tattoo extends BaseObj {
    private String name;
    private BigDecimal price;
    private String state;
    private int userId;
    private List<Rating> ratingList;

    public Tattoo() {
    }

    public Tattoo(int id, String name, BigDecimal price,
                  String state, int userId, List<Rating> ratingList) {
        super(id);
        this.name = name;
        this.price = price;
        this.state = state;
        this.userId = userId;
        this.ratingList = ratingList;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public List<Rating> getRatingList() {
        return ratingList;
    }

    public void setRatingList(List<Rating> ratingList) {
        this.ratingList = ratingList;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Tattoo)) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }

        Tattoo tattoo = (Tattoo) o;

        if (userId != tattoo.userId) {
            return false;
        }
        if (name != null ? !name.equals(tattoo.name) : tattoo.name != null) {
            return false;
        }
        if (price != null
                ? !(price.compareTo(tattoo.price) == 0)
                : tattoo.price != null) {
            return false;
        }
        if (state != null ? !state.equals(tattoo.state)
                : tattoo.state != null) {
            return false;
        }
        return ratingList != null ? ratingList.equals(tattoo.ratingList)
                : tattoo.ratingList == null;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (price != null ? price.hashCode() : 0);
        result = 31 * result + (state != null ? state.hashCode() : 0);
        result = 31 * result + userId;
        result = 31 * result + (ratingList != null ? ratingList.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Tattoo{" +
                "name='" + name + '\'' +
                ", price=" + price +
                ", state='" + state + '\'' +
                ", userId=" + userId +
                ", ratingList=" + ratingList +
                "} " + super.toString();
    }
}

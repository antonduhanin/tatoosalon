package by.epam.tatoosalon.entity.pojo;

import by.epam.tatoosalon.entity.BaseObj;

import java.util.Arrays;

public class Image extends BaseObj {
    private String path;
    private int tatooId;
    private String base64Encoded;

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Image)) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }

        Image image = (Image) o;

        if (tatooId != image.tatooId) {
            return false;
        }
        if (path != null ? !path.equals(image.path) : image.path != null) {
            return false;
        }
        return base64Encoded != null ? base64Encoded.equals(image.base64Encoded) : image.base64Encoded == null;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (path != null ? path.hashCode() : 0);
        result = 31 * result + tatooId;
        result = 31 * result + (base64Encoded != null ? base64Encoded.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Image{" +
                "path='" + path + '\'' +
                ", tatooId=" + tatooId +
                ", base64Encoded='" + base64Encoded + '\'' +
                "} " + super.toString();
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public int getTatooId() {
        return tatooId;
    }

    public void setTatooId(int tatooId) {
        this.tatooId = tatooId;
    }

    public String getBase64Encoded() {
        return base64Encoded;
    }

    public void setBase64Encoded(String base64Encoded) {
        this.base64Encoded = base64Encoded;
    }
}

package by.epam.tatoosalon.entity.pojo;

import by.epam.tatoosalon.entity.BaseObj;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

public class Order extends BaseObj {
    private int userId;
    private BigDecimal totalPrice;
    private Date date;
    private boolean isPaid;
    private List<WorkInfo> workInfo;

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public BigDecimal getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(BigDecimal totalPrice) {
        this.totalPrice = totalPrice;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public boolean isPaid() {
        return isPaid;
    }

    public void setPaid(boolean paid) {
        isPaid = paid;
    }

    public List<WorkInfo> getWorkInfo() {
        return workInfo;
    }

    public void setWorkInfo(List<WorkInfo> workInfo) {
        this.workInfo = workInfo;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Order)) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }

        Order order = (Order) o;

        if (userId != order.userId) {
            return false;
        }
        if (isPaid != order.isPaid) {
            return false;
        }
        if (totalPrice != null
                ? !(totalPrice.compareTo(order.totalPrice) == 0)
                : order.totalPrice != null) {
            return false;
        }
        if (date != null
                ? !(date.getTime() == order.date.getTime())
                : order.date != null) {
            return false;
        }
        return workInfo != null ? workInfo.equals(order.workInfo)
                : order.workInfo == null;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + userId;
        result = 31 * result + (totalPrice != null ? totalPrice.hashCode() : 0);
        result = 31 * result + (date != null ? date.hashCode() : 0);
        result = 31 * result + (isPaid ? 1 : 0);
        result = 31 * result + (workInfo != null ? workInfo.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Order{" +
                "userId=" + userId +
                ", totalPrice=" + totalPrice +
                ", date=" + date +
                ", isPaid=" + isPaid +
                ", workInfo=" + workInfo +
                "} " + super.toString();
    }
}

package by.epam.tatoosalon.entity.pojo;

public class Rating {
    private int userId;
    private int tatooId;
    private int value;

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getTatooId() {
        return tatooId;
    }

    public void setTatooId(int tatooId) {
        this.tatooId = tatooId;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Rating)) {
            return false;
        }

        Rating rating = (Rating) o;

        if (userId != rating.userId) {
            return false;
        }
        if (tatooId != rating.tatooId) {
            return false;
        }
        return value == rating.value;
    }

    @Override
    public int hashCode() {
        int result = userId;
        result = 31 * result + tatooId;
        result = 31 * result + value;
        return result;
    }

    @Override
    public String toString() {
        return "Rating{" +
                "userId=" + userId +
                ", tatooId=" + tatooId +
                ", value=" + value +
                '}';
    }
}

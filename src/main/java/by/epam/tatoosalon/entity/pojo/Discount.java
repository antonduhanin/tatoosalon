package by.epam.tatoosalon.entity.pojo;

import by.epam.tatoosalon.entity.BaseObj;

import java.math.BigDecimal;
import java.util.Date;

public class Discount extends BaseObj {
    private String name;
    private BigDecimal value;
    private Date endDiscount;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getValue() {
        return value;
    }

    public void setValue(BigDecimal value) {
        this.value = value;
    }

    public Date getEndDiscount() {
        return endDiscount;
    }

    public void setEndDiscount(Date endDiscount) {
        this.endDiscount = endDiscount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Discount)) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }

        Discount discount = (Discount) o;

        if (name != null ? !name.equals(discount.name)
                : discount.name != null) {
            return false;
        }
        if (value != null ? !(value.compareTo(discount.value) == 0)
                : discount.value != null) {
            return false;
        }
        return endDiscount != null
                ? endDiscount.getTime() == endDiscount.getTime()
                : discount.endDiscount == null;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (value != null ? value.hashCode() : 0);
        result = 31 * result + (endDiscount != null
                ? endDiscount.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Discount{" +
                "name='" + name + '\'' +
                ", value=" + value +
                ", endDiscount=" + endDiscount +
                "} " + super.toString();
    }
}

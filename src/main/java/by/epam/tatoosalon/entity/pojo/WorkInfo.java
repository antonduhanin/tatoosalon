package by.epam.tatoosalon.entity.pojo;

public class WorkInfo {
    private int orderId;
    private int tattooId;
    private String nameMaster;
    private int ratingWork;

    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    public int getTattooId() {
        return tattooId;
    }

    public void setTattooId(int tattooId) {
        this.tattooId = tattooId;
    }

    public String getNameMaster() {
        return nameMaster;
    }

    public void setNameMaster(String nameMaster) {
        this.nameMaster = nameMaster;
    }

    public int getRatingWork() {
        return ratingWork;
    }

    public void setRatingWork(int ratingWork) {
        this.ratingWork = ratingWork;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof WorkInfo)) {
            return false;
        }

        WorkInfo workInfo = (WorkInfo) o;

        if (orderId != workInfo.orderId) {
            return false;
        }
        if (tattooId != workInfo.tattooId) {
            return false;
        }
        if (ratingWork != workInfo.ratingWork) {
            return false;
        }
        return nameMaster != null ? nameMaster.equals(workInfo.nameMaster)
                : workInfo.nameMaster == null;
    }

    @Override
    public int hashCode() {
        int result = orderId;
        result = 31 * result + tattooId;
        result = 31 * result + (nameMaster != null ? nameMaster.hashCode() : 0);
        result = 31 * result + ratingWork;
        return result;
    }

    @Override
    public String toString() {
        return "WorkInfo{" +
                "orderId=" + orderId +
                ", tattooId=" + tattooId +
                ", nameMaster='" + nameMaster + '\'' +
                ", ratingWork=" + ratingWork +
                '}';
    }
}

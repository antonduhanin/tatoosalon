package by.epam.tatoosalon.exception;

public class ActionException extends Exception {
    public ActionException() {
    }

    public ActionException(String message) {
        super(message);
    }
}

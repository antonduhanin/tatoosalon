package by.epam.tatoosalon.action.security;

import java.util.HashMap;

import static by.epam.tatoosalon.action.constant.ActionConstants.*;

public class SecurityConfig {
    private static final HashMap<String, String> URL_ROLE = new HashMap<>();

    static {
        URL_ROLE.put("/admin.html", ADMIN_ROLE);
        URL_ROLE.put("/rate.html", CLIENT_ROLE);
        URL_ROLE.put("/logout.html", COMMON_ROLE);
        URL_ROLE.put("/tattoo.html", COMMON_ROLE);

    }

    public static HashMap<String, String> getUrlRole() {
        return URL_ROLE;
    }
}

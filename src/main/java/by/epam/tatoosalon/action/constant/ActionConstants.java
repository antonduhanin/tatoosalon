package by.epam.tatoosalon.action.constant;

public class ActionConstants {
    public static final String CLIENT_ROLE = "client";
    public static final String ADMIN_ROLE = "administrator";
    public static final String COMMON_ROLE = "all";
    public static final String PROPERTIES_FILE = "/action.properties";

    private ActionConstants() {
    }
}

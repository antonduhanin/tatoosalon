package by.epam.tatoosalon.action;

import by.epam.tatoosalon.action.security.SecurityConfig;
import by.epam.tatoosalon.entity.dto.UserDto;
import by.epam.tatoosalon.exception.ActionException;
import by.epam.tatoosalon.service.ServiceFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;

import static by.epam.tatoosalon.action.constant.ActionConstants.COMMON_ROLE;

abstract public class Action {
    private UserDto authorizedUser;
    private String name;

    protected ServiceFactory factory;

    public UserDto getAuthorizedUser() {
        return authorizedUser;
    }

    public void setAuthorizedUser(UserDto authorizedUser) {
        this.authorizedUser = authorizedUser;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setFactory(ServiceFactory factory) {
        this.factory = factory;
    }

    abstract public Action.Forward exec(HttpServletRequest request,
                                        HttpServletResponse response)
            throws ActionException;

    public static class Forward {
        private String forward;
        private boolean redirect;
        private Map<String, Object> attributes = new HashMap<>();

        public Forward(String forward, boolean redirect) {
            this.forward = forward;
            this.redirect = redirect;
        }

        public Forward(String forward) {
            this(forward, true);
        }

        public String getForward() {
            return forward;
        }

        public void setForward(String forward) {
            this.forward = forward;
        }

        public boolean isRedirect() {
            return redirect;
        }

        public void setRedirect(boolean redirect) {
            this.redirect = redirect;
        }

        public Map<String, Object> getAttributes() {
            return attributes;
        }
    }

    public boolean getAccess(UserDto user, String url) {
        boolean access = false;
        String roleName = SecurityConfig.getUrlRole().get(url);
        String userRole = user.getRole().getName();
        if (userRole.equals(roleName) || COMMON_ROLE.equals(roleName)) {
            access = true;
        }
        return access;

    }
}

package by.epam.tatoosalon.action.utils;

import by.epam.tatoosalon.action.constant.ActionConstants;
import by.epam.tatoosalon.action.impl.common.AddTattooAction;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class ActionUtils {
    public static  String getPath() throws IOException {
        InputStream inputStream = AddTattooAction.class
                .getResourceAsStream(ActionConstants.PROPERTIES_FILE);
        Properties properties = new Properties();
        properties.load(inputStream);
        return (String) properties.get("PATH_TO_IMAGES");
    }
}

package by.epam.tatoosalon.action.impl.guest;

import by.epam.tatoosalon.action.Action;
import by.epam.tatoosalon.action.constant.ActionConstants;
import by.epam.tatoosalon.entity.builder.UserBuilder;
import by.epam.tatoosalon.entity.builder.impl.UserBuilderImpl;
import by.epam.tatoosalon.entity.dto.UserDto;
import by.epam.tatoosalon.exception.ActionException;
import by.epam.tatoosalon.exception.ServiceException;
import by.epam.tatoosalon.service.RoleService;
import by.epam.tatoosalon.service.UserService;
import by.epam.tatoosalon.validator.ValidatorRegistration;
import by.epam.tatoosalon.validator.impl.ValidatorRegistrationImpl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class RegistrationAction extends Action {

    @Override
    public Forward exec(HttpServletRequest request,
                        HttpServletResponse response) throws ActionException {
        String login = request.getParameter("login");
        String password = request.getParameter("password");
        if (getAuthorizedUser() != null) {
            return new Forward("/logout.html");
        }
        if (checkLoginAndPassword(request, login, password)) {
            try {
                UserService userService = factory.getService(UserService.class);
                UserDto user = userService.findByLogin(login);
                if (user == null) {
                    user = createUser(login, password);
                    int id = userService.save(user);
                    user.setId(id);
                    HttpSession session = request.getSession();
                    session.setAttribute("authorizedUser", user);
                    return new Forward("/index.html");
                }
            } catch (ServiceException e) {
                throw new ActionException();
            }
            request.setAttribute("message", "user with this login exists");
        }
        return null;
    }

    private UserDto createUser(String login, String password)
            throws ServiceException {
        UserBuilder userBuilder = new UserBuilderImpl();
        RoleService roleService = factory.getService(RoleService.class);
        userBuilder.setLogin(login);
        userBuilder.setPassword(password);
        return userBuilder.buildClient(roleService,
                ActionConstants.CLIENT_ROLE);
    }

    private boolean checkLoginAndPassword(HttpServletRequest request, String
            login, String password) {
        String confirmLogin = request.getParameter("confirmLogin");
        String confirmPassword = request.getParameter("confirmPassword");
        ValidatorRegistration validatorRegistration
                = new ValidatorRegistrationImpl(login, confirmLogin,
                password, confirmPassword);
        return validatorRegistration.validData();
    }

}

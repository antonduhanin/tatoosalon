package by.epam.tatoosalon.action.impl;

import by.epam.tatoosalon.action.Action;
import by.epam.tatoosalon.action.ActionManager;
import by.epam.tatoosalon.exception.ActionException;
import by.epam.tatoosalon.service.ServiceFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class ActionManagerImpl implements ActionManager {
    private ServiceFactory serviceFactory;

    public ActionManagerImpl(ServiceFactory serviceFactory) {
        this.serviceFactory = serviceFactory;
    }

    @Override
    public Action.Forward execute(Action action, HttpServletRequest request, HttpServletResponse response) throws ActionException {
        action.setFactory(serviceFactory);
        return action.exec(request, response);
    }
}

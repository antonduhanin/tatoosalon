package by.epam.tatoosalon.action.impl;

import by.epam.tatoosalon.action.ActionManager;
import by.epam.tatoosalon.service.ServiceFactory;

public class ActionManagerFactory {
    public static ActionManager getManager(ServiceFactory factory) {
        return new ActionManagerImpl(factory);
    }
}

package by.epam.tatoosalon.action.impl.guest;

import by.epam.tatoosalon.action.Action;
import by.epam.tatoosalon.action.utils.ActionUtils;
import by.epam.tatoosalon.entity.dto.TattooDto;
import by.epam.tatoosalon.entity.pojo.Tattoo;
import by.epam.tatoosalon.exception.ActionException;
import by.epam.tatoosalon.exception.ServiceException;
import by.epam.tatoosalon.service.TattooService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

public class MainAction extends Action {
    @Override
    public Forward exec(HttpServletRequest request,
                        HttpServletResponse response) throws ActionException {
        try {
            TattooService tatooService
                    = factory.getService(TattooService.class);
            List<TattooDto> tatooList = tatooService.findPublishedTattoo();
            String imagePath = ActionUtils.getPath();
            tatooList.forEach(tattooDto -> tattooDto
                    .getImageList()
                    .forEach(image -> image
                            .setPath(imagePath + image.getPath())));
            request.setAttribute("tattooList", tatooList);
            if (getAuthorizedUser() != null) {
                request.setAttribute("loginState", "logout");
            } else {
                request.setAttribute("loginState", "login");
            }
            return null;
        } catch (ServiceException | IOException e) {
            throw new ActionException();
        }
    }
}

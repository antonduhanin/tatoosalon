package by.epam.tatoosalon.action.impl.guest;

import by.epam.tatoosalon.action.Action;
import by.epam.tatoosalon.entity.dto.UserDto;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class LogoutAction extends Action {
    @Override
    public Forward exec(HttpServletRequest request,
                        HttpServletResponse response) {
        UserDto user = getAuthorizedUser();
        //logger.info(String.format("user \"%s\" is logged out", user.getLogin()));
        request.getSession(false).invalidate();
        return new Forward("/login.html");
    }
}

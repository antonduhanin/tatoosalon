package by.epam.tatoosalon.action.impl.common;

import by.epam.tatoosalon.action.Action;
import by.epam.tatoosalon.action.constant.ActionConstants;
import by.epam.tatoosalon.action.utils.ActionUtils;
import by.epam.tatoosalon.entity.builder.TattoBuilder;
import by.epam.tatoosalon.entity.builder.impl.TattooBuilderImpl;
import by.epam.tatoosalon.entity.pojo.Image;
import by.epam.tatoosalon.entity.pojo.Tattoo;
import by.epam.tatoosalon.exception.ActionException;
import by.epam.tatoosalon.exception.ServiceException;
import by.epam.tatoosalon.service.TattooService;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

import static by.epam.tatoosalon.action.constant.ActionConstants.ADMIN_ROLE;

public class AddTattooAction extends Action {
    private Map<String, String> parameters = new HashMap<>();
    private List<String> pathList = new ArrayList<>();

    @Override
    public Forward exec(HttpServletRequest request,
                        HttpServletResponse response) throws ActionException {

        String contentType = request.getContentType();
        if (contentType != null
                && contentType.startsWith("multipart/form-data")) {
            try {
                // Create a factory for disk-based file items
                DiskFileItemFactory diskFileItemFactory = new DiskFileItemFactory();
                // Create a new file upload handler
                ServletFileUpload upload = new ServletFileUpload(diskFileItemFactory);
                List<FileItem> items = upload.parseRequest(request);
                handleParametersFromRequest(items);
                createTattoo();
                request.setAttribute("message", "successfully added");
            } catch (FileUploadException | IOException e) {
                throw new ActionException(e.getMessage());
            } catch (ServiceException e) {
                throw new ActionException("service exception " + e.getMessage());
            }
        } else {
            String userRole = getAuthorizedUser().getRole().getName();
            if (userRole.equals(ADMIN_ROLE)) {
                request.setAttribute("roleAdmin", "true");
            }
        }
        return null;
    }


    private void createTattoo() throws ServiceException {
        TattoBuilder tattooBuilder = new TattooBuilderImpl();
        tattooBuilder.setName(parameters.get("nameTattoo"));
        tattooBuilder.setUserId(getAuthorizedUser().getId());
        String price = parameters.get("priceTattoo");
        if (price != null) {
            tattooBuilder.setPrice(new BigDecimal(price));
        }
        String state = parameters.get("stateTattoo");
        if (state != null) {
            tattooBuilder.setState(state);
        }
        TattooService tattooService = factory.getService(TattooService.class);
        List<Image> images = pathList.stream().map(path -> {
            Image image = new Image();
            image.setPath(path);
            return image;
        }).collect(Collectors.toList());
        tattooService.createTattoo(tattooBuilder.buildTattooDto(), images);
    }

    private void handleParametersFromRequest(List<FileItem> items)
            throws IOException {
        for (FileItem item : items) {
            if (item.isFormField()) {
                parameters.put(item.getFieldName(), item.getString());
            } else {
                processUploadedFile(item);
            }
        }
    }

    private void processUploadedFile(FileItem item)
            throws IOException {
        String filename = FilenameUtils.getName(item.getName());
        InputStream fileContent = item.getInputStream();
        if (!filename.equals("")) {
            Long date = new Date().getTime();
            filename = date.toString() + filename;
            String contentPath = ActionUtils.getPath();
            File file = new File(contentPath + filename);
            FileUtils.copyInputStreamToFile(fileContent, file);
            pathList.add(filename);
        }
    }
}

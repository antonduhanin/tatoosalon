package by.epam.tatoosalon.action.impl.client;

import by.epam.tatoosalon.action.Action;
import by.epam.tatoosalon.entity.dto.UserDto;
import by.epam.tatoosalon.exception.ActionException;
import by.epam.tatoosalon.exception.ServiceException;
import by.epam.tatoosalon.service.TattooService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class RateTatoo extends Action {
    @Override
    public Forward exec(HttpServletRequest request,
                        HttpServletResponse response) throws ActionException {
        Integer rateValue = (Integer) request.getAttribute("rateValue");
        if (rateValue == null) {
            rateValue = Integer.parseInt(request.getParameter("rateValue"));
        }

        Integer tatooId = (Integer) request.getAttribute("tattooId");
        if (tatooId == null) {
            tatooId = Integer.parseInt(request.getParameter("tattooId"));
        }
        try {
            TattooService tattooService
                    = factory.getService(TattooService.class);
            UserDto user = getAuthorizedUser();
            tattooService.rateTattoo(tatooId, user.getId(), rateValue);
            return new Forward("/index.html");
        } catch (ServiceException e) {
            throw new ActionException();
        }
    }
}

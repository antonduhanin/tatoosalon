package by.epam.tatoosalon.action.impl.guest;

import by.epam.tatoosalon.action.Action;
import by.epam.tatoosalon.entity.dto.UserDto;
import by.epam.tatoosalon.exception.ActionException;
import by.epam.tatoosalon.exception.ServiceException;
import by.epam.tatoosalon.service.UserService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class LoginAction extends Action {
    @Override
    public Forward exec(HttpServletRequest request,
                        HttpServletResponse response) throws ActionException {
        String login = request.getParameter("login");
        String password = request.getParameter("password");
        if (login != null && password != null) {
            try {
                UserService service = factory.getService(UserService.class);
                UserDto user = service.findByLoginAndPassword(login, password);
                if (user != null) {
                    HttpSession session = request.getSession();
                    session.setAttribute("authorizedUser", user);
                    return new Forward("/index.html");
                } else {
                    request.setAttribute("message", "user not found");
                }
            } catch (ServiceException e) {
                throw new ActionException();
            }
        }
        return null;
    }
}

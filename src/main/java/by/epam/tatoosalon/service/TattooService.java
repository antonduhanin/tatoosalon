package by.epam.tatoosalon.service;

import by.epam.tatoosalon.entity.dto.TattooDto;
import by.epam.tatoosalon.entity.pojo.Image;
import by.epam.tatoosalon.entity.pojo.Tattoo;
import by.epam.tatoosalon.exception.ServiceException;

import java.util.List;

public interface TattooService extends Service {
    List<TattooDto> findAll() throws ServiceException;

    List<TattooDto> findPublishedTattoo() throws ServiceException;

    void createTattoo(TattooDto tattoo, List<Image> images)
            throws ServiceException;

    void rateTattoo(int tatooId, int userId, int rateValue)
            throws ServiceException;
}

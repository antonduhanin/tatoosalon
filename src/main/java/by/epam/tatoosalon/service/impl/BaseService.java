package by.epam.tatoosalon.service.impl;

import by.epam.tatoosalon.dao.DaoManager;
import by.epam.tatoosalon.service.Service;

abstract public class BaseService implements Service {
    protected DaoManager daoManager;

    public void setDaoManager(DaoManager daoManager) {
        this.daoManager = daoManager;
    }
}

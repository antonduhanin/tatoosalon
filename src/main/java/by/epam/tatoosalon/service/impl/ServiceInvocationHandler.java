package by.epam.tatoosalon.service.impl;

import by.epam.tatoosalon.exception.DaoException;
import by.epam.tatoosalon.exception.ServiceException;
import by.epam.tatoosalon.service.constant.ServiceConstants;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class ServiceInvocationHandler implements InvocationHandler {
    private BaseService service;

    public ServiceInvocationHandler(BaseService service) {
        this.service = service;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        try {
            Object result = method.invoke(service, args);
            if (service.daoManager.isTransaction()) {
                service.daoManager.commit();
            }
            service.daoManager.closeConnection();
            return result;
        } catch (IllegalAccessException | InvocationTargetException | DaoException e) {
            if (service.daoManager.isTransaction()) {
                rollback(method);
            }
            throw new ServiceException(ServiceConstants.TRANSACTION_FAILED);
        }

    }

    private void rollback(Method method) {
        try {
            service.daoManager.rollback();
        } catch (DaoException e) {

        }
    }
}
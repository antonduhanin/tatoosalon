package by.epam.tatoosalon.service.impl.converters.impl;

import by.epam.tatoosalon.action.utils.ActionUtils;
import by.epam.tatoosalon.dao.DaoManager;
import by.epam.tatoosalon.dao.ImageDao;
import by.epam.tatoosalon.entity.builder.TattoBuilder;
import by.epam.tatoosalon.entity.dto.TattooDto;
import by.epam.tatoosalon.entity.pojo.Image;
import by.epam.tatoosalon.entity.pojo.Rating;
import by.epam.tatoosalon.entity.pojo.Tattoo;
import by.epam.tatoosalon.exception.DaoException;
import by.epam.tatoosalon.exception.ServiceException;
import by.epam.tatoosalon.service.impl.converters.ConverterTattoo;
import org.apache.commons.io.IOUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Base64;
import java.util.IntSummaryStatistics;
import java.util.List;

public class ConverterTattooImpl implements ConverterTattoo {
    private TattoBuilder tattoBuilder;

    public ConverterTattooImpl(TattoBuilder userBuilder) {
        this.tattoBuilder = userBuilder;
    }

    @Override
    public Tattoo tattooToEntity(TattooDto tattooDto) {
        tattoBuilder.setId(tattooDto.getId());
        tattoBuilder.setName(tattooDto.getName());
        tattoBuilder.setUserId(tattooDto.getUserId());
        tattoBuilder.setPrice(tattooDto.getPrice());
        tattoBuilder.setState(tattooDto.getState());
        tattoBuilder.setRatingList(tattooDto.getRatingList());
        return tattoBuilder.buildTattoo();
    }

    @Override
    public TattooDto tattooToDto(Tattoo tattoo, DaoManager daoManager)
            throws ServiceException {
        tattoBuilder.setId(tattoo.getId());
        tattoBuilder.setName(tattoo.getName());
        tattoBuilder.setUserId(tattoo.getUserId());
        tattoBuilder.setPrice(tattoo.getPrice());
        tattoBuilder.setState(tattoo.getState());
        tattoBuilder.setRatingList(tattoo.getRatingList());
        try {
            ImageDao imageDao = daoManager.createDao(ImageDao.class);
            List<Image> imageList = imageDao.getImageByTattoo(tattoo.getId());
            initImages(imageList);
            tattoBuilder.setImageList(imageList);
        } catch (DaoException e) {
            throw new ServiceException();
        }
        IntSummaryStatistics ratingStatistics = tattoo.getRatingList()
                .stream()
                .mapToInt(Rating::getValue)
                .summaryStatistics();
        tattoBuilder.setRatingStatistics(ratingStatistics);
        return tattoBuilder.buildTattooDto();
    }

    private void initImages(List<Image> images) {
        for (Image image : images) {
            try {
                File file = new File(ActionUtils.getPath() + image.getPath());
                InputStream inputStream = new FileInputStream(file);
                byte[] bytes = IOUtils.toByteArray(inputStream);
                byte[] encodeBase64 = Base64.getEncoder().encode(bytes);
                String base64Encoded = new String(encodeBase64, "UTF-8");
                image.setBase64Encoded(base64Encoded);
            } catch (IOException e) {
            }
        }
    }
}

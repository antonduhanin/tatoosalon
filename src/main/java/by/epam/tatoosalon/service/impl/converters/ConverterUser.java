package by.epam.tatoosalon.service.impl.converters;

import by.epam.tatoosalon.dao.DaoManager;
import by.epam.tatoosalon.entity.dto.UserDto;
import by.epam.tatoosalon.entity.pojo.User;
import by.epam.tatoosalon.exception.ServiceException;

public interface ConverterUser {
    User userToEntity(UserDto userDto);

    UserDto userToDto(User user, DaoManager daoManager) throws ServiceException;
}

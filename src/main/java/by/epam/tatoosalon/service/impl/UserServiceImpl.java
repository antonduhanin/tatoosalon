package by.epam.tatoosalon.service.impl;

import by.epam.tatoosalon.dao.UserDao;
import by.epam.tatoosalon.entity.builder.impl.UserBuilderImpl;
import by.epam.tatoosalon.entity.dto.UserDto;
import by.epam.tatoosalon.entity.pojo.User;
import by.epam.tatoosalon.exception.DaoException;
import by.epam.tatoosalon.exception.ServiceException;
import by.epam.tatoosalon.service.UserService;
import by.epam.tatoosalon.service.impl.converters.ConverterUser;
import by.epam.tatoosalon.service.impl.converters.impl.ConverterUserImpl;

import java.util.ArrayList;
import java.util.List;

public class UserServiceImpl extends BaseService implements UserService {
    private UserDao userDao;
    private final ConverterUser converterUser;

    public UserServiceImpl() {
        converterUser = new ConverterUserImpl(new UserBuilderImpl());
    }

    @Override
    public List<UserDto> findAll() throws ServiceException {
        try {
            userDao = daoManager.createDao(UserDao.class);
            List<User> users = userDao.getAll();
            List<UserDto> userDtoList = new ArrayList<>();
            for (User user : users) {
                UserDto userDto = converterUser.userToDto(user, daoManager);
                userDtoList.add(userDto);
            }
            return userDtoList;
        } catch (DaoException e) {
            throw new ServiceException();
        }
    }

    @Override
    public UserDto findById(int id) throws ServiceException {
        try {
            userDao = daoManager.createDao(UserDao.class);
            User user = userDao.read(id);
            return converterUser.userToDto(user, daoManager);
        } catch (DaoException e) {
            throw new ServiceException();
        }
    }

    @Override
    public UserDto findByLoginAndPassword(String login, String password) throws
            ServiceException {
        try {
            userDao = daoManager.createDao(UserDao.class);
            User user = userDao.read(login, password);
            if (user != null) {
                return converterUser.userToDto(user, daoManager);
            }
            return null;
        } catch (DaoException e) {
            throw new ServiceException();
        }
    }

    @Override
    public int save(UserDto userDto) throws ServiceException {
        try {

            User user = converterUser.userToEntity(userDto);
            daoManager.setTransaction(true);
            userDao = daoManager.createDao(UserDao.class);
            return userDao.create(user);
        } catch (DaoException e) {
            throw new ServiceException();
        }
    }

    @Override
    public void delete(int id) throws ServiceException {
        try {
            daoManager.setTransaction(true);
            userDao = daoManager.createDao(UserDao.class);
            userDao.delete(id);
        } catch (DaoException e) {
            throw new ServiceException();
        }
    }

    @Override
    public UserDto findByLogin(String login) throws ServiceException {
        try {
            userDao = daoManager.createDao(UserDao.class);
            User user = userDao.readByLogin(login);
            if (user != null) {
                return converterUser.userToDto(user, daoManager);
            }
            return null;
        } catch (DaoException e) {
            throw new ServiceException();
        }
    }
}

package by.epam.tatoosalon.service.impl.converters;

import by.epam.tatoosalon.dao.DaoManager;
import by.epam.tatoosalon.entity.dto.TattooDto;
import by.epam.tatoosalon.entity.pojo.Tattoo;
import by.epam.tatoosalon.exception.ServiceException;

public interface ConverterTattoo {
    Tattoo tattooToEntity(TattooDto userDto);

    TattooDto tattooToDto(Tattoo tattoo, DaoManager daoManager)
            throws ServiceException;
}

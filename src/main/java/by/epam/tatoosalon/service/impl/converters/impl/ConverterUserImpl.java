package by.epam.tatoosalon.service.impl.converters.impl;

import by.epam.tatoosalon.dao.DaoManager;
import by.epam.tatoosalon.dao.RoleDao;
import by.epam.tatoosalon.entity.builder.UserBuilder;
import by.epam.tatoosalon.entity.dto.UserDto;
import by.epam.tatoosalon.entity.pojo.Role;
import by.epam.tatoosalon.entity.pojo.User;
import by.epam.tatoosalon.exception.DaoException;
import by.epam.tatoosalon.exception.ServiceException;
import by.epam.tatoosalon.service.impl.converters.ConverterUser;

public class ConverterUserImpl implements ConverterUser {
    private UserBuilder userBuilder;

    public ConverterUserImpl(UserBuilder userBuilder) {
        this.userBuilder = userBuilder;
    }

    public User userToEntity(UserDto userDto) {
        userBuilder.setId(userDto.getId());
        userBuilder.setLogin(userDto.getLogin());
        userBuilder.setRoleId(userDto.getRole().getId());
        userBuilder.setPassword(userDto.getPassword());
        return userBuilder.buildUser();
    }

    public UserDto userToDto(User user, DaoManager daoManager)
            throws ServiceException {
        userBuilder.setPassword(user.getPassword());
        userBuilder.setLogin(user.getLogin());
        userBuilder.setId(user.getId());
        try {
            RoleDao roleDao = daoManager.createDao(RoleDao.class);
            Role role = roleDao.read(user.getRoleId());
            userBuilder.setRole(role);
            return userBuilder.buildUserDto();
        } catch (DaoException e) {
            throw new ServiceException();
        }
    }
}

package by.epam.tatoosalon.service.impl;

import by.epam.tatoosalon.dao.RoleDao;
import by.epam.tatoosalon.entity.pojo.Role;
import by.epam.tatoosalon.exception.DaoException;
import by.epam.tatoosalon.exception.ServiceException;
import by.epam.tatoosalon.service.RoleService;

import java.util.List;

public class RoleServiceImpl extends BaseService implements RoleService {
    private RoleDao roleDao;

    @Override
    public List<Role> findAll() throws ServiceException {
        try {
            roleDao = daoManager.createDao(RoleDao.class);
            return roleDao.getAll();
        } catch (DaoException e) {
            throw new ServiceException();
        }
    }

    @Override
    public Role findById(int id) throws ServiceException {
        try {
            roleDao = daoManager.createDao(RoleDao.class);
            return roleDao.read(id);
        } catch (DaoException e) {
            throw new ServiceException();
        }
    }

    @Override
    public Role findByName(String name) throws ServiceException {
        try {
            roleDao = daoManager.createDao(RoleDao.class);
            return roleDao.readByName(name);
        } catch (DaoException e) {
            throw new ServiceException();
        }
    }
}

package by.epam.tatoosalon.service.impl;

import by.epam.tatoosalon.dao.ImageDao;
import by.epam.tatoosalon.dao.TattooDao;
import by.epam.tatoosalon.entity.builder.impl.TattooBuilderImpl;
import by.epam.tatoosalon.entity.dto.TattooDto;
import by.epam.tatoosalon.entity.enums.TattooState;
import by.epam.tatoosalon.entity.pojo.Image;
import by.epam.tatoosalon.entity.pojo.Tattoo;
import by.epam.tatoosalon.exception.DaoException;
import by.epam.tatoosalon.exception.ServiceException;
import by.epam.tatoosalon.service.TattooService;
import by.epam.tatoosalon.service.impl.converters.ConverterTattoo;
import by.epam.tatoosalon.service.impl.converters.impl.ConverterTattooImpl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class TattooServiceImpl extends BaseService implements TattooService {
    private TattooDao tattooDao;
    private ConverterTattoo converterTattoo;

    public TattooServiceImpl() {
        converterTattoo = new ConverterTattooImpl(new TattooBuilderImpl());
    }

    @Override
    public List<TattooDto> findAll() throws ServiceException {
        try {
            tattooDao = daoManager.createDao(TattooDao.class);
            List<TattooDto> tattooDtoList = new ArrayList<>();
            for (Tattoo tattoo : tattooDao.getAll()) {
                TattooDto tattooDto = converterTattoo
                        .tattooToDto(tattoo, daoManager);
                tattooDtoList.add(tattooDto);
            }
            return tattooDtoList;
        } catch (DaoException e) {
            throw new ServiceException();
        }
    }

    @Override
    public List<TattooDto> findPublishedTattoo() throws ServiceException {
        try {
            tattooDao = daoManager.createDao(TattooDao.class);
            List<Tattoo> tattooList = tattooDao.getAll();
            tattooList = tattooList
                    .stream()
                    .filter(tattoo -> tattoo.getState()
                            .equals(TattooState.PUBLISHED.getState()))
                    .collect(Collectors.toList());
            List<TattooDto> tattooDtoList = new ArrayList<>();
            for (Tattoo tattoo : tattooList) {
                TattooDto tattooDto = converterTattoo
                        .tattooToDto(tattoo, daoManager);
                tattooDtoList.add(tattooDto);
            }
            return tattooDtoList;
        } catch (DaoException e) {
            throw new ServiceException();
        }
    }

    @Override
    public void createTattoo(TattooDto tattooDto,
                             List<Image> images) throws ServiceException {
        try {
            daoManager.setTransaction(true);
            tattooDao = daoManager.createDao(TattooDao.class);
            ImageDao imageDao = daoManager.createDao(ImageDao.class);
            if (tattooDto.getState() == null) {
                tattooDto.setState(TattooState.WAIT.getState());
            }
            if (tattooDto.getPrice() == null) {
                tattooDto.setPrice(new BigDecimal(0));
            }
            Tattoo tattoo = converterTattoo.tattooToEntity(tattooDto);
            int tattooId = tattooDao.create(tattoo);
            for (Image image : images) {
                image.setTatooId(tattooId);
                imageDao.create(image);
            }
        } catch (DaoException e) {
            throw new ServiceException();
        }
    }

    @Override
    public void rateTattoo(int tatooId, int userId, int rateValue)
            throws ServiceException {
        try {
            daoManager.setTransaction(true);
            tattooDao = daoManager.createDao(TattooDao.class);
            tattooDao.rateTattoo(tatooId, userId, rateValue);
        } catch (DaoException e) {
            throw new ServiceException();
        }
    }
}

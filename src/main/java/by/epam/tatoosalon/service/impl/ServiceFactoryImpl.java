package by.epam.tatoosalon.service.impl;

import by.epam.tatoosalon.dao.DaoManager;
import by.epam.tatoosalon.exception.DaoException;
import by.epam.tatoosalon.exception.ServiceException;
import by.epam.tatoosalon.service.*;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Proxy;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class ServiceFactoryImpl implements ServiceFactory {
    private static final Map<Class<? extends Service>, Class<? extends BaseService>> SERVICES = new ConcurrentHashMap<>();

    static {
        SERVICES.put(UserService.class, UserServiceImpl.class);
        SERVICES.put(TattooService.class, TattooServiceImpl.class);
        SERVICES.put(RoleService.class, RoleServiceImpl.class);

    }

    private DaoManager daoManager;

    public ServiceFactoryImpl(DaoManager daoManager) {
        this.daoManager = daoManager;
    }

    @Override
    @SuppressWarnings("unchecked")
    public <Type extends Service> Type getService(Class<Type> key) throws ServiceException {
        Class<? extends BaseService> value = SERVICES.get(key);
        if (value != null) {
            try {
                ClassLoader classLoader = value.getClassLoader();
                Class<?>[] interfaces = {key};
                BaseService service = value.newInstance();
                service.setDaoManager(daoManager);
                daoManager.openConnection();
                InvocationHandler handler = new ServiceInvocationHandler(service);
                return (Type) Proxy.newProxyInstance(classLoader, interfaces, handler);
            } catch (IllegalAccessException | InstantiationException  |
                    DaoException e) {
                throw new ServiceException("can not to be get service: "
                        + e.getMessage());
            }
        } else {
            throw new ServiceException("not found impl of service: ");
        }
    }
}

package by.epam.tatoosalon.service;

import by.epam.tatoosalon.entity.dto.UserDto;
import by.epam.tatoosalon.exception.ServiceException;

import java.util.List;

public interface UserService extends Service {
    List<UserDto> findAll() throws ServiceException;

    UserDto findById(int id) throws ServiceException;

    UserDto findByLoginAndPassword(String login, String password)
            throws ServiceException;

    int save(UserDto user) throws ServiceException;

    void delete(int id) throws ServiceException;

    UserDto findByLogin(String login) throws ServiceException;
}

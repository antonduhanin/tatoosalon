package by.epam.tatoosalon.service;

import by.epam.tatoosalon.exception.ServiceException;

public interface ServiceFactory {
    <Type extends Service> Type getService(Class<Type> key) throws ServiceException;
}

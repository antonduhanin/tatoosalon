package by.epam.tatoosalon.service;

import by.epam.tatoosalon.entity.pojo.Role;
import by.epam.tatoosalon.exception.ServiceException;

import java.util.List;

public interface RoleService extends Service {
    List<Role> findAll() throws ServiceException;

    Role findById(int id) throws ServiceException;

    Role findByName(String name)throws ServiceException;
}

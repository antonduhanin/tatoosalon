package by.epam.tatoosalon.controller.filter;

import by.epam.tatoosalon.action.Action;
import by.epam.tatoosalon.action.impl.guest.MainAction;
import by.epam.tatoosalon.action.security.SecurityConfig;
import by.epam.tatoosalon.entity.dto.UserDto;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class SecurityFilter implements Filter {
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        if (servletRequest instanceof HttpServletRequest && servletResponse instanceof HttpServletResponse) {
            HttpServletRequest httpRequest = (HttpServletRequest) servletRequest;
            HttpServletResponse httpResponse = (HttpServletResponse) servletResponse;
            Action action = (Action) httpRequest.getAttribute("action");
            String userName = "unauthorized user";
            HttpSession session = httpRequest.getSession(false);
            UserDto user = null;
            if (session != null) {
                user = (UserDto) session.getAttribute("authorizedUser");
                action.setAuthorizedUser(user);
                String errorMessage = (String) session.getAttribute("SecurityFilterMessage");
                if (errorMessage != null) {
                    httpRequest.setAttribute("message", errorMessage);
                    session.removeAttribute("SecurityFilterMessage");
                }
            }
            String uri = httpRequest.getRequestURI();
            boolean canExecute = true;
            if (SecurityConfig.getUrlRole().get(uri) != null) {
                canExecute = false;
                if (user != null) {
                    canExecute = action.getAccess(user, uri);
                }
            }

            if (canExecute) {
                filterChain.doFilter(servletRequest, servletResponse);
            } else {
                if (session != null && action.getClass() != MainAction.class) {
                    session.setAttribute("SecurityFilterMessage", "access is not available");
                }
                httpResponse.sendRedirect(httpRequest.getContextPath() + "/login.html");

            }
        } else {
            servletRequest.getServletContext().getRequestDispatcher("/WEB-INF/jsp/error.jsp").forward(servletRequest, servletResponse);

        }
    }

    @Override
    public void destroy() {

    }
}

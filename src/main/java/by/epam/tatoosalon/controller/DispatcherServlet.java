package by.epam.tatoosalon.controller;

import by.epam.tatoosalon.action.Action;
import by.epam.tatoosalon.action.ActionManager;
import by.epam.tatoosalon.action.impl.ActionManagerFactory;
import by.epam.tatoosalon.dao.mysql.DaoManagerImpl;
import by.epam.tatoosalon.exception.ActionException;
import by.epam.tatoosalon.exception.ServiceException;
import by.epam.tatoosalon.service.ServiceFactory;
import by.epam.tatoosalon.service.impl.ServiceFactoryImpl;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class DispatcherServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        process(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        process(req, resp);
    }

    private void process(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Action action = (Action) req.getAttribute("action");
        try {
            ActionManager actionManager = ActionManagerFactory.getManager(getFactory());
            Action.Forward forward = actionManager.execute(action, req, resp);
            if (forward != null && forward.isRedirect()) {
                String redirectedUri = req.getContextPath() + forward.getForward();
                resp.sendRedirect(redirectedUri);
            } else {
                String jspPage;
                if (forward != null) {
                    jspPage = forward.getForward();
                } else {
                    jspPage = action.getName() + ".jsp";
                }
                jspPage = "/WEB-INF/jsp" + jspPage;
                getServletContext()
                        .getRequestDispatcher(jspPage)
                        .forward(req, resp);
            }
        } catch (ServiceException | ActionException e) {
            req.setAttribute("error", "Ошибка обработки данных");
            getServletContext().getRequestDispatcher("/WEB-INF/jsp/error.jsp").forward(req, resp);
        }
    }

    private ServiceFactory getFactory() throws ServiceException {
        return new ServiceFactoryImpl(new DaoManagerImpl());
    }
}

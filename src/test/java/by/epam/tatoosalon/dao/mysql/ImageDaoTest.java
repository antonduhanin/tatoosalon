package by.epam.tatoosalon.dao.mysql;

import by.epam.tatoosalon.dao.Dao;
import by.epam.tatoosalon.dao.pool.ConnectionPoolImpl;
import by.epam.tatoosalon.entity.pojo.Image;
import by.epam.tatoosalon.exception.DaoException;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.sql.SQLException;

public class ImageDaoTest extends DaoTest {
    private Dao<Image> imageDao;

    @BeforeTest
    public void setUp() throws DaoException, SQLException {
        pool = ConnectionPoolImpl.getInstance();
        connection = pool.checkOut();
        connection.setAutoCommit(false);
        imageDao = new ImageDaoImpl();
        ((ImageDaoImpl) imageDao).setConnection(connection);
    }

    @Test
    public void testCreate() throws SQLException {
        Image image = new Image();
        image.setPath("path");
        image.setTatooId(1);
        try {
            int id = imageDao.create(image);
            image.setId(id);
            Assert.assertEquals(image, imageDao.read(id), "images not equals");
        } catch (DaoException e) {
            Assert.fail("exception creating new writing in db: " + e);
        } finally {
            connection.rollback();
        }
    }
}
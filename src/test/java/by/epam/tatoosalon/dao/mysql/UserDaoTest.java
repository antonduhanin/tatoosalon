package by.epam.tatoosalon.dao.mysql;

import by.epam.tatoosalon.dao.UserDao;
import by.epam.tatoosalon.dao.pool.ConnectionPoolImpl;
import by.epam.tatoosalon.entity.pojo.User;
import by.epam.tatoosalon.exception.DaoException;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.sql.SQLException;

public class UserDaoTest extends DaoTest {

    private UserDao userDao;

    @BeforeTest
    public void setUp() throws SQLException, DaoException {
        pool = ConnectionPoolImpl.getInstance();
        connection = pool.checkOut();
        connection.setAutoCommit(false);
        userDao = new UserDaoImpl();
        ((UserDaoImpl) userDao).setConnection(connection);
    }

    @Test
    public void testCreate() throws SQLException {
        User user = new User();
        user.setLogin("user");
        user.setPassword("pass");
        user.setRoleId(1);
        try {
            int id = userDao.create(user);
            user.setId(id);
            Assert.assertEquals(user, userDao.read(id));
        } catch (DaoException e) {
            Assert.fail("exception creating new writing in db: " + e);
        } finally {
            connection.rollback();
        }
    }

    @Test
    public void testRead() {
        try {
            User user = userDao.read(1);
            Assert.assertNotNull(user);
        } catch (DaoException e) {
            Assert.fail("exception reading from db: " + e);
        }
    }

    @Test
    public void findByLogin() {
        String login = "user@gmail.com";
        try {
            User user = userDao.readByLogin(login);
            Assert.assertNotNull(user);

        } catch (DaoException e) {
            Assert.fail("exception reading from db: " + e);
        }

    }

    @Test
    public void testUpdate() {
    }

    @Test
    public void testDelete() {
    }
}
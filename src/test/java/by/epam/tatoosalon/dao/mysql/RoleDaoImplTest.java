package by.epam.tatoosalon.dao.mysql;

import by.epam.tatoosalon.dao.RoleDao;
import by.epam.tatoosalon.dao.pool.ConnectionPoolImpl;
import by.epam.tatoosalon.exception.DaoException;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.sql.SQLException;

public class RoleDaoImplTest extends DaoTest {

    private RoleDao roleDao;

    @BeforeMethod
    public void setUp() throws SQLException, DaoException {
        pool = ConnectionPoolImpl.getInstance();
        connection = pool.checkOut();
        connection.setAutoCommit(false);
        roleDao = new RoleDaoImpl();
        ((RoleDaoImpl) roleDao).setConnection(connection);
    }

    @Test
    public void testReadByName() {
        try {
            roleDao.readByName("client");
        } catch (DaoException e) {
            Assert.fail("exception reading row in db: " + e);
        }
    }
}
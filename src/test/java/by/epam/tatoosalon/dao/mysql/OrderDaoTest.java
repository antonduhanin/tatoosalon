package by.epam.tatoosalon.dao.mysql;

import by.epam.tatoosalon.dao.OrderDao;
import by.epam.tatoosalon.dao.pool.ConnectionPoolImpl;
import by.epam.tatoosalon.entity.pojo.Order;
import by.epam.tatoosalon.exception.DaoException;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;

public class OrderDaoTest extends DaoTest {

    private OrderDao orderDao;

    @BeforeTest
    public void setUp() throws SQLException, DaoException {
        pool = ConnectionPoolImpl.getInstance();
        connection = pool.checkOut();
        connection.setAutoCommit(false);
        orderDao = new OrderDaoImpl();
        ((OrderDaoImpl) orderDao).setConnection(connection);
    }

    @Test
    public void testCreate() throws SQLException {
        Order order = new Order();
        order.setUserId(1);
        Date date = new Date(1537045200000l);
        order.setDate(date);
        order.setPaid(false);
        order.setTotalPrice(new BigDecimal(12.00));
        order.setWorkInfo(new ArrayList<>());
        try {
            int id = orderDao.create(order);
            order.setId(id);
            Assert.assertEquals(order, orderDao.read(id), "orders not equals");
        } catch (DaoException e) {
            Assert.fail("exception creating new writing in db: " + e);
        } finally {
            connection.rollback();
        }
    }

    @Test
    public void testRead() {
        try {
            Order order = orderDao.read(1);
            Assert.assertNotNull(order);
        } catch (DaoException e) {
            Assert.fail("exception reading from db: " + e);
        }
    }

    @Test
    public void testUpdate() {
        Order order = new Order();
        Order order2 = new Order();
        Assert.assertEquals(order,order2);
    }

    @Test
    public void testDelete() {
    }
}
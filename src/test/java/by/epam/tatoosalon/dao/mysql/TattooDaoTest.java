package by.epam.tatoosalon.dao.mysql;

import by.epam.tatoosalon.dao.TattooDao;
import by.epam.tatoosalon.dao.pool.ConnectionPoolImpl;
import by.epam.tatoosalon.entity.pojo.Tattoo;
import by.epam.tatoosalon.exception.DaoException;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.ArrayList;

public class TattooDaoTest extends DaoTest {
    private TattooDao tatooDao;

    @BeforeTest
    public void setUp() throws SQLException, DaoException {
        pool = ConnectionPoolImpl.getInstance();
        connection = pool.checkOut();
        connection.setAutoCommit(false);
        tatooDao = new TattooDaoImpl();
        ((TattooDaoImpl) tatooDao).setConnection(connection);
    }

    @Test
    public void testCreate() throws SQLException {
        Tattoo tattoo = new Tattoo();
        tattoo.setName("test");
        tattoo.setUserId(1);
        tattoo.setPrice(new BigDecimal(2));
        tattoo.setState("wait");
        tattoo.setRatingList(new ArrayList<>());
        try {
            int id = tatooDao.create(tattoo);
            tattoo.setId(id);
            Assert.assertEquals(tattoo, tatooDao.read(id), "tatoo not equals");
        } catch (DaoException e) {
            Assert.fail("exception creating new writing in db: " + e);
        } finally {
            connection.rollback();
        }
    }

    @Test
    public void testRead() {
    }

    @Test
    public void testUpdate() {
    }

    @Test
    public void testDelete() {
    }

    @Test
    public void createRateTatoo() throws SQLException {
        try {
            tatooDao.rateTattoo(5, 1, 3);
        } catch (DaoException e) {
            Assert.fail("exception creating new writing in db: " + e);
        }finally {
            connection.rollback();
        }
    }
}
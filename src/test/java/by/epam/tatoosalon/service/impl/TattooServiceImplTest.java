package by.epam.tatoosalon.service.impl;

import by.epam.tatoosalon.dao.DaoManager;
import by.epam.tatoosalon.dao.mysql.DaoManagerImpl;
import by.epam.tatoosalon.exception.ServiceException;
import by.epam.tatoosalon.service.ServiceFactory;
import by.epam.tatoosalon.service.TattooService;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import static org.testng.Assert.*;

public class TattooServiceImplTest extends ServiceTest {
    TattooService tattooService;

    @BeforeTest
    public void setUp() throws ServiceException {
        DaoManager daoManager = new DaoManagerImpl();
        //daoManager.setTransaction(true);
        ServiceFactory serviceFactory = new ServiceFactoryImpl(daoManager);
        tattooService = serviceFactory.getService(TattooService.class);
    }

    @Test
    public void testRateTattoo() {
        try {
            tattooService.rateTattoo(2,1,4);
        } catch (ServiceException e) {
            Assert.fail("exception creating new writing in db: " + e);
        }
    }
}
package by.epam.tatoosalon.service.impl;

import by.epam.tatoosalon.dao.DaoManager;
import by.epam.tatoosalon.dao.mysql.DaoManagerImpl;
import by.epam.tatoosalon.exception.DaoException;
import by.epam.tatoosalon.exception.ServiceException;
import by.epam.tatoosalon.service.ServiceFactory;
import by.epam.tatoosalon.service.UserService;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class UserServiceImplTest extends ServiceTest {
    UserService userService;

    @BeforeTest
    public void setUp() throws  ServiceException {
        DaoManager daoManager = new DaoManagerImpl();
        ServiceFactory serviceFactory = new ServiceFactoryImpl(daoManager);
        userService = serviceFactory.getService(UserService.class);
    }

    @Test()
    public void testFindAll() {
        try {
            Assert.assertNotNull(userService.findAll());
        } catch (ServiceException e) {
            Assert.fail("exception in service: " + e);
        }
    }

    @Test
    public void testFindByLogin() {
        String login = "user@gmail.comasd";
        try {
            Assert.assertNull(userService.findByLogin(login));
        } catch (ServiceException e) {
            Assert.fail("exception in service: " + e);
        }
    }
}
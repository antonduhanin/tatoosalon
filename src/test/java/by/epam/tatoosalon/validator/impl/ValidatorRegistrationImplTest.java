package by.epam.tatoosalon.validator.impl;

import by.epam.tatoosalon.dao.DaoManager;
import by.epam.tatoosalon.dao.mysql.DaoManagerImpl;
import by.epam.tatoosalon.exception.ServiceException;
import by.epam.tatoosalon.service.ServiceFactory;
import by.epam.tatoosalon.service.TattooService;
import by.epam.tatoosalon.service.impl.ServiceFactoryImpl;
import by.epam.tatoosalon.validator.ValidatorRegistration;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static org.testng.Assert.*;

public class ValidatorRegistrationImplTest {
    private String login;
    private String confirmLogin;
    private String password;
    private String confirmPassword;
    private ValidatorRegistration validator;

    @DataProvider(name = "loginsAndPasswords")
    public static Object[][] getLoginsAndPasswords() {
        return new Object[][]{{"user@domain.com", "user@domain.com",
                "testtest2", "testtest2"},
                {"user@domain.co.in", "user@domain.co.in",
                        "Test123", "Test123"}};
    }

    @Test(testName = "validateData", dataProvider = "loginsAndPasswords")
    public void testValidData(String login, String confirmLogin,
                              String password, String confirmPassword) {
        validator = new ValidatorRegistrationImpl(login, confirmLogin,
                password, confirmPassword);
        Assert.assertTrue(validator.validData());
    }

    @DataProvider(name = "wrongloginsAndPasswords")
    public static Object[][] getWrongLoginsAndPasswords() {
        return new Object[][]{{".username@yahoo.com", ".username@yahoo.com",
                "testtest2", "testtest2"},
                {"", "", "Test123", "Test123"},
                {"user@domain.com", "user@domain.com", "", ""},
                {null, null, null, null},};
    }

    @Test(testName = "validateWrongData", dataProvider = "loginsAndPasswords")
    public void testValidWrongData(String login, String confirmLogin,
                                   String password, String confirmPassword) {
        validator = new ValidatorRegistrationImpl(login, confirmLogin,
                password, confirmPassword);
        Assert.assertTrue(validator.validData());
    }
}
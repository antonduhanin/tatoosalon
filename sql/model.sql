SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema tatooSalonDB
-- -----------------------------------------------------
-- Предметная область: Tatoo-салон.
-- Клиент имеет возможность осуществлять поиск и заказ Изображений в Каталоге, а также выставлять им оценку, в том числе и за качество работы. Клиент может предлагать свои ихображения салону, за что может получать скидки. Администратор управляет Клиентами и контентом системы.

-- -----------------------------------------------------
-- Schema tatooSalonDB
--
-- Предметная область: Tatoo-салон.
-- Клиент имеет возможность осуществлять поиск и заказ Изображений в Каталоге, а также выставлять им оценку, в том числе и за качество работы. Клиент может предлагать свои ихображения салону, за что может получать скидки. Администратор управляет Клиентами и контентом системы.
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `tatooSalonDB` DEFAULT CHARACTER SET utf8 ;
USE `tatooSalonDB` ;

-- -----------------------------------------------------
-- Table `tatooSalonDB`.`role`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `tatooSalonDB`.`role` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'синтетический первичный ключ для таблицы Role.',
  `name` VARCHAR(20) NOT NULL COMMENT 'уникальное название для роли',
  PRIMARY KEY (`id`),
  UNIQUE INDEX `title_UNIQUE` (`name` ASC))
ENGINE = InnoDB
COMMENT = 'Таблица role хранит роли для данной системы';


-- -----------------------------------------------------
-- Table `tatooSalonDB`.`user`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `tatooSalonDB`.`user` (git rm -r --cached myFolder
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Синтетический первичный ключ для таблицы user',
  `login` VARCHAR(254) NOT NULL COMMENT 'уникальное имя пользователя в системе, так же является email, чем обусловлен размер в 254 символа.',
  `login` VARCHAR(64) NOT NULL COMMENT 'хэш для пароля',
  `role_id` INT UNSIGNED NOT NULL COMMENT 'foreign key для таблицы role, связывающий данную таблицу с таблицей role.',
  PRIMARY KEY (`id`),
  INDEX `fk_user_role1_idx` (`role_id` ASC),
  UNIQUE INDEX `login_UNIQUE` (`login` ASC),
  CONSTRAINT `fk_user_role1`
    FOREIGN KEY (`role_id`)
    REFERENCES `tatooSalonDB`.`role` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
COMMENT = 'Таблица user хранит описание сущности User.';


-- -----------------------------------------------------
-- Table `tatooSalonDB`.`tatoo`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `tatooSalonDB`.`tatoo` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'синтетический первичный ключ для таблицы tatoo.',
  `name` VARCHAR(100) NOT NULL COMMENT 'уникальное название тату',
  `price` DECIMAL(9,2) NULL COMMENT 'стоимость для татуировки.',
  `state` ENUM('published', 'approved', 'wait') NOT NULL DEFAULT 'wait' COMMENT 'состояния данной татуировки,\npublished - опубликована\napproved - одобрена администратором, но не опубликована\nwait - ожидание одобрения',
  `user_id` INT UNSIGNED NULL COMMENT 'foreign key, ссылающийся на юзера, который предложил данную татуировку салону.',
  PRIMARY KEY (`id`),
  INDEX `fk_tatoo_user1_idx` (`user_id` ASC),
  UNIQUE INDEX `name_UNIQUE` (`name` ASC),
  CONSTRAINT `fk_tatoo_user1`
    FOREIGN KEY (`user_id`)
    REFERENCES `tatooSalonDB`.`user` (`id`)
    ON DELETE SET NULL
    ON UPDATE NO ACTION)
ENGINE = InnoDB
COMMENT = 'Таблица tatoo хранит описание каждой татуировки и id_user который предложил данную тату.';


-- -----------------------------------------------------
-- Table `tatooSalonDB`.`image`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `tatooSalonDB`.`image` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'синтетический первичный ключ для таблицы image',
  `path` VARCHAR(255) NOT NULL COMMENT 'путь до картинки.',
  `tatoo_id` INT UNSIGNED NOT NULL COMMENT 'Картинка должна ссылаться на какое-либо тату.',
  PRIMARY KEY (`id`),
  INDEX `fk_image_tatoo1_idx` (`tatoo_id` ASC),
  CONSTRAINT `fk_image_tatoo1`
    FOREIGN KEY (`tatoo_id`)
    REFERENCES `tatooSalonDB`.`tatoo` (`id`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB
COMMENT = 'Таблица image создана для хранения путей картинок для тату.';


-- -----------------------------------------------------
-- Table `tatooSalonDB`.`rating`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `tatooSalonDB`.`rating` (
  `user_id` INT UNSIGNED NOT NULL COMMENT 'т.к. таблица реализует связь многие ко многим между таблицами user и tatoo, то имеет составной первичный ключ user_id и tatoo_id ',
  `tatoo_id` INT UNSIGNED NOT NULL COMMENT 'т.к. таблица реализует связь многие ко многим между таблицами user и tatoo, то имеет составной первичный ключ user_id и tatoo_id',
  `value` TINYINT(5) NOT NULL COMMENT 'значение оценки для татуировки, ограничено 5 баллами',
  PRIMARY KEY (`user_id`, `tatoo_id`),
  INDEX `fk_user_has_tatoo_tatoo1_idx` (`tatoo_id` ASC),
  INDEX `fk_user_has_tatoo_user_idx` (`user_id` ASC),
  CONSTRAINT `fk_user_has_tatoo_user`
    FOREIGN KEY (`user_id`)
    REFERENCES `tatooSalonDB`.`user` (`id`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_user_has_tatoo_tatoo1`
    FOREIGN KEY (`tatoo_id`)
    REFERENCES `tatooSalonDB`.`tatoo` (`id`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB
COMMENT = 'Таблица rating хранит значение  рейтинга татуировки оцененной пользователем. ';


-- -----------------------------------------------------
-- Table `tatooSalonDB`.`order`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `tatooSalonDB`.`order` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'синтетический первичный ключ для таблицы order.',
  `user_id` INT UNSIGNED NOT NULL COMMENT 'foreign key для связи с таблицей user, то есть чей это заказ.',
  `total_price` DECIMAL(9,2) NOT NULL COMMENT 'общая сумма для заказа',
  `date` DATETIME NOT NULL COMMENT 'дата заказа.',
  `is_paid` TINYINT(1) NOT NULL DEFAULT 0,
  INDEX `fk_tatoo_has_user_user1_idx` (`user_id` ASC),
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_tatoo_has_user_user1`
    FOREIGN KEY (`user_id`)
    REFERENCES `tatooSalonDB`.`user` (`id`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB
COMMENT = 'В таблице order хранятся заказы пользователей на тату.';


-- -----------------------------------------------------
-- Table `tatooSalonDB`.`order_tatoo`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `tatooSalonDB`.`order_tatoo` (
  `order_id` INT UNSIGNED NOT NULL COMMENT 'таблица order_tatoo является связующей для реализации связи многие ко многим между таблицами order и tatoo и имеет составной первичный ключ order_id и tattoo_id',
  `tatoo_id` INT UNSIGNED NOT NULL,
  `rating_work` TINYINT(5) NULL COMMENT 'Содержит оценку о качестве выполненной работы.',
  `name_master` VARCHAR(45) NULL COMMENT 'имя мастера, который выполнил часть заказа(order_id) на тату (tatoo_id) ',
  PRIMARY KEY (`order_id`, `tatoo_id`),
  INDEX `fk_order_has_tatoo_tatoo1_idx` (`tatoo_id` ASC),
  CONSTRAINT `fk_order_has_tatoo_order1`
    FOREIGN KEY (`order_id`)
    REFERENCES `tatooSalonDB`.`order` (`id`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_order_has_tatoo_tatoo1`
    FOREIGN KEY (`tatoo_id`)
    REFERENCES `tatooSalonDB`.`tatoo` (`id`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB
COMMENT = 'Связующая таблица для связи многи ко многим между таблицами order и tatoo, которая так же влючает дополнительные поля содержащие информацию о выполненной работе.';


-- -----------------------------------------------------
-- Table `tatooSalonDB`.`discount`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `tatooSalonDB`.`discount` (
  `id` INT UNSIGNED NOT NULL COMMENT 'Синтетический первичный ключ для таблицы discont.',
  `name` VARCHAR(100) NOT NULL COMMENT 'Название данной скидки( должно быть уникально).',
  `value` DECIMAL(5,2) NOT NULL COMMENT 'значение скидки.',
  PRIMARY KEY (`id`),
  UNIQUE INDEX `name_UNIQUE` (`name` ASC))
ENGINE = InnoDB
COMMENT = 'Таблица содержащая информацию о скидках для пользователей.';


-- -----------------------------------------------------
-- Table `tatooSalonDB`.`user_discount`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `tatooSalonDB`.`user_discount` (
  `user_id` INT UNSIGNED NOT NULL COMMENT 'т.к. таблица связующая для отношения многие ко многим между discount и user, то имеет составной первичный ключ из user_id и discount_id',
  `discount_id` INT UNSIGNED NOT NULL COMMENT 'т.к. таблица связующая для отношения многие ко многим между discount и user, то имеет составной первичный ключ из user_id и discount_id',
  `end_discount` DATETIME NULL COMMENT 'Дата окончания действия скидки для пользователя.',
  PRIMARY KEY (`user_id`, `discount_id`),
  INDEX `fk_user_has_discount_discount1_idx` (`discount_id` ASC),
  INDEX `fk_user_has_discount_user1_idx` (`user_id` ASC),
  CONSTRAINT `fk_user_has_discount_user1`
    FOREIGN KEY (`user_id`)
    REFERENCES `tatooSalonDB`.`user` (`id`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_user_has_discount_discount1`
    FOREIGN KEY (`discount_id`)
    REFERENCES `tatooSalonDB`.`discount` (`id`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB
COMMENT = 'Связующая таблица для связи многие ко многим между таблицами discont и user';


-- -----------------------------------------------------
-- Procedure for update rating
-- -----------------------------------------------------
 DELIMITER //
 CREATE PROCEDURE rateTatoo
 (
 inputTatooId int,
 inputUserId int,
 inputRating tinyint(5)
 )
   BEGIN
 	if exists(Select 1 From rating Where tatoo_id = inputTatooId and user_id =  inputUserId) then
        update rating set `value` = inputRating where tatoo_id = inputTatooId and user_id =  inputUserId;
    ELSE
        INSERT INTO rating (tatoo_id,user_id,`value`) VALUES ( inputTatooId, inputUserId, inputRating);
       END IF;
   END; //
 DELIMITER ;

-- -----------------------------------------------------
-- Procedure for update rating o work
-- -----------------------------------------------------
  DELIMITER //
 CREATE PROCEDURE rateWork
 (
 inputTatooId int,
 inputOrderId int,
 inputRating tinyint(5)
 )
   BEGIN
 	if exists(Select 1 From order_tatoo Where tatoo_id = inputTatooId and order_id =  inputOrderId) then
        update rating set `value` = inputRating where tatoo_id = inputTatooId and order_id =  inputOrderId;
    ELSE
        INSERT INTO order_tatoo (tatoo_id,order_id,`value`) VALUES ( inputTatooId, inputOrderId, inputRating);
       END IF;
   END; //
 DELIMITER ;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

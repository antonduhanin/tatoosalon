/*
запросы where
*/

/*
выбрать все логины начинающиеся с a
*/
select login from user where user.login regexp "^a"; 
-- ---------------------------------------------------
/*выбрать скидки значения которых больше 5 но меньше 10*/
select name,value from discount where value>5 and value<10;
-- -------------------------------------------------


/*
запросы с встроенными функциями
*/

/*
выбрать только действующие скидки для пользователей
*/
select user_id,end_discount from user_discount where end_discount>now();
-- ---------------------------------------------------------------------
/*
выбрать тату в названии которых есть слово "tatoo"
*/
select name from tatoo where LOCATE("tatoo",name);
-- -----------------------------------------------------------------------

/*
запросы на соеденения таблиц
*/


/*
запрос на выборку всех оценок соответсвующих пользователю с id =1
*/
select value,tatoo_id from rating inner join user on user_id=user.id where user.id =1;
-- -------------------------------------------------------------------------------
/*
запрос на выборку всех пользователей и тату которые они предложили тату салону
*/
select user.login,tatoo.name,tatoo.state from user left join tatoo on user.id=tatoo.user_id;

-- --------------------------------------------------------------------------------


/*
запрос с агрегатными функциями 
*/

/*
запрос выводящий количество предолженных тату пользователем, если количество тату больше 1
*/
select user.login,count(tatoo.id) as number_tatoo from user inner join tatoo on user.id=tatoo.user_id group by(user_id) having number_tatoo>1 ;
-- --------------------------------------------------
/*запрос выводящий количество пользователей каждой роли*/
select count(user.id) as number_users_with_same_role,role.name  from user inner join role on role.id=user.role_id  group by (user.role_id);

-- ---------------------------------------------------------------------------------------------------------
/*
показать общую сумму таких заказов пользователя, которые меньше максимальной стоимости одного заказа.
*/
select sum(total_price) as summa from tatoosalondb.`order` group by(user_id) having (select max(total_price) from tatoosalondb.`order`)>summa;
-- ---------------------------------------------------------------------------------------------------------



/*запрос с union*/

/*запрос для вывода всего рейтинга для тату с id=1*/
select value as all_rating_tatoo from rating where tatoo_id=1
union select rating_work from order_tatoo where tatoo_id=1;


-- ----------------------------------------------------
/*
запрос с подзапросом
*/
/*
выбрать пользователей, которые ставили оценки
*/
select 
	user.login 
from 
	user 
where 
	user.id = 
		(
        select 
			distinct user_id 
		from 
			rating 
		where 
			user.id = rating.user_id
        );


/*
запрос на выборку тату цена которых меньше средней(невзаимосвязный)
*/
select
	name,
    price 
from 
	tatoo 
where
	price<
    (
	select 
		avg(price)
	from 
		tatoo
	);   

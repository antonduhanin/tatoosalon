/*
-- Query: SELECT * FROM tatoosalondb.role
LIMIT 0, 1000

-- Date: 2018-08-29 16:29
*/
INSERT INTO `role` (`id`,`name`) VALUES (2,'administrator');
INSERT INTO `role` (`id`,`name`) VALUES (1,'client');

/*
-- Query: SELECT * FROM tatoosalondb.user
LIMIT 0, 1000

-- Date: 2018-08-29 16:30
*/
INSERT INTO `user` (`id`,`login`,`password`,`role_id`) VALUES (1,'user@gmail.com','123456',1);
INSERT INTO `user` (`id`,`login`,`password`,`role_id`) VALUES (2,'client@tut.by','4444',1);
INSERT INTO `user` (`id`,`login`,`password`,`role_id`) VALUES (3,'mainClient@gmail.com','5555',1);
INSERT INTO `user` (`id`,`login`,`password`,`role_id`) VALUES (4,'admin@tut.by','1111',2);
INSERT INTO `user` (`id`,`login`,`password`,`role_id`) VALUES (5,'mainAdmin@gmail.com','3333',2);
INSERT INTO `user` (`id`,`login`,`password`,`role_id`) VALUES (6,'client@yandex.ru','121211',1);
INSERT INTO `user` (`id`,`login`,`password`,`role_id`) VALUES (7,'iamuser.tut.by','1111',1);
INSERT INTO `user` (`id`,`login`,`password`,`role_id`) VALUES (8,'anton@gamil.com','2323',1);
INSERT INTO `user` (`id`,`login`,`password`,`role_id`) VALUES (9,'john@tut.by','123',2);
INSERT INTO `user` (`id`,`login`,`password`,`role_id`) VALUES (10,'jack.gmail.com','1231',1);


/*
-- Query: SELECT * FROM tatoosalondb.tatoo
LIMIT 0, 1000

-- Date: 2018-08-29 16:29
*/
INSERT INTO `tatoo` (`id`,`name`,`price`,`state`,`user_id`) VALUES (1,'my tatoo',1.23,'published',2);
INSERT INTO `tatoo` (`id`,`name`,`price`,`state`,`user_id`) VALUES (2,'tatoo mainClient',4.23,'wait',3);
INSERT INTO `tatoo` (`id`,`name`,`price`,`state`,`user_id`) VALUES (3,'first tatoo',23232.33,'approved',2);
INSERT INTO `tatoo` (`id`,`name`,`price`,`state`,`user_id`) VALUES (4,'best tatoo',4.44,'published',5);
INSERT INTO `tatoo` (`id`,`name`,`price`,`state`,`user_id`) VALUES (5,'castle tatoo',44.44,'approved',3);
INSERT INTO `tatoo` (`id`,`name`,`price`,`state`,`user_id`) VALUES (6,'random tatoo',2.12,'wait',2);
INSERT INTO `tatoo` (`id`,`name`,`price`,`state`,`user_id`) VALUES (7,'simple',1.21,'published',6);
INSERT INTO `tatoo` (`id`,`name`,`price`,`state`,`user_id`) VALUES (8,'most popular tatoo',2.00,'published',6);
INSERT INTO `tatoo` (`id`,`name`,`price`,`state`,`user_id`) VALUES (9,'special',444.00,'published',7);


/*
-- Query: SELECT * FROM tatoosalondb.image
LIMIT 0, 1000

-- Date: 2018-08-29 16:27
*/
INSERT INTO `image` (`id`,`path`,`tatoo_id`) VALUES (1,'www.example.com',1);
INSERT INTO `image` (`id`,`path`,`tatoo_id`) VALUES (2,'www.example.com',1);
INSERT INTO `image` (`id`,`path`,`tatoo_id`) VALUES (3,'www.example.com',2);
INSERT INTO `image` (`id`,`path`,`tatoo_id`) VALUES (4,'www.example.com',3);
INSERT INTO `image` (`id`,`path`,`tatoo_id`) VALUES (5,'www.example.com',4);
INSERT INTO `image` (`id`,`path`,`tatoo_id`) VALUES (6,'www.example.com',5);
INSERT INTO `image` (`id`,`path`,`tatoo_id`) VALUES (7,'www.example.com',8);
INSERT INTO `image` (`id`,`path`,`tatoo_id`) VALUES (8,'www.example.com',9);
INSERT INTO `image` (`id`,`path`,`tatoo_id`) VALUES (9,'www.example.com',7);



/*
-- Query: SELECT * FROM tatoosalondb.discount
LIMIT 0, 1000

-- Date: 2018-08-29 16:36
*/
INSERT INTO `discount` (`id`,`name`,`value`) VALUES (1,'new client',5.00);
INSERT INTO `discount` (`id`,`name`,`value`) VALUES (2,'offer tatoo',10.00);
INSERT INTO `discount` (`id`,`name`,`value`) VALUES (3,'new year',7.00);
INSERT INTO `discount` (`id`,`name`,`value`) VALUES (4,'birthday',15.00);
INSERT INTO `discount` (`id`,`name`,`value`) VALUES (5,'old client',7.00);
INSERT INTO `discount` (`id`,`name`,`value`) VALUES (6,'free',100.00);


/*
-- Query: SELECT * FROM tatoosalondb.user_discount
LIMIT 0, 1000

-- Date: 2018-08-29 16:30
*/
INSERT INTO `user_discount` (`user_id`,`discount_id`,`end_discount`) VALUES (1,2,'2018-11-10 00:00:00');
INSERT INTO `user_discount` (`user_id`,`discount_id`,`end_discount`) VALUES (1,3,'2017-12-31 00:00:00');
INSERT INTO `user_discount` (`user_id`,`discount_id`,`end_discount`) VALUES (2,2,'2018-06-06 00:00:00');
INSERT INTO `user_discount` (`user_id`,`discount_id`,`end_discount`) VALUES (3,4,'2018-09-09 00:00:00');
INSERT INTO `user_discount` (`user_id`,`discount_id`,`end_discount`) VALUES (6,2,'2018-12-12 00:00:00');
INSERT INTO `user_discount` (`user_id`,`discount_id`,`end_discount`) VALUES (7,5,'2018-12-12 00:00:00');
INSERT INTO `user_discount` (`user_id`,`discount_id`,`end_discount`) VALUES (8,4,'2018-11-12 00:00:00');
INSERT INTO `user_discount` (`user_id`,`discount_id`,`end_discount`) VALUES (10,1,'2018-05-05 00:00:00');


/*
-- Query: SELECT * FROM tatoosalondb.`order`
LIMIT 0, 1000

-- Date: 2018-08-29 16:28
*/
INSERT INTO `order` (`id`,`user_id`,`total_price`,`date`,`is_paid`) VALUES (1,2,5.46,'2018-06-09 00:00:00',1);
INSERT INTO `order` (`id`,`user_id`,`total_price`,`date`,`is_paid`) VALUES (2,3,44.44,'2018-07-10 00:00:00',0);
INSERT INTO `order` (`id`,`user_id`,`total_price`,`date`,`is_paid`) VALUES (3,2,4.44,'2018-07-23 00:00:00',1);
INSERT INTO `order` (`id`,`user_id`,`total_price`,`date`,`is_paid`) VALUES (4,10,3.21,'2018-08-28 00:00:00',1);
INSERT INTO `order` (`id`,`user_id`,`total_price`,`date`,`is_paid`) VALUES (5,8,1.21,'2018-07-07 00:00:00',1);
INSERT INTO `order` (`id`,`user_id`,`total_price`,`date`,`is_paid`) VALUES (6,8,2.12,'2018-08-08 00:00:00',1);


/*
-- Query: SELECT * FROM tatoosalondb.order_tatoo
LIMIT 0, 1000

-- Date: 2018-08-29 16:28
*/
INSERT INTO `order_tatoo` (`order_id`,`tatoo_id`,`rating_work`,`name_master`) VALUES (2,5,NULL,'start master');
INSERT INTO `order_tatoo` (`order_id`,`tatoo_id`,`rating_work`,`name_master`) VALUES (1,1,2,'best master');
INSERT INTO `order_tatoo` (`order_id`,`tatoo_id`,`rating_work`,`name_master`) VALUES (1,2,2,'norm master');
INSERT INTO `order_tatoo` (`order_id`,`tatoo_id`,`rating_work`,`name_master`) VALUES (4,7,5,'best');
INSERT INTO `order_tatoo` (`order_id`,`tatoo_id`,`rating_work`,`name_master`) VALUES (4,8,5,NULL);
INSERT INTO `order_tatoo` (`order_id`,`tatoo_id`,`rating_work`,`name_master`) VALUES (5,7,4,'best');
INSERT INTO `order_tatoo` (`order_id`,`tatoo_id`,`rating_work`,`name_master`) VALUES (6,6,4,'norm master');


/*
-- Query: SELECT * FROM tatoosalondb.rating
LIMIT 0, 1000

-- Date: 2018-08-29 16:29
*/
INSERT INTO `rating` (`user_id`,`tatoo_id`,`value`) VALUES (1,2,4);
INSERT INTO `rating` (`user_id`,`tatoo_id`,`value`) VALUES (1,3,5);
INSERT INTO `rating` (`user_id`,`tatoo_id`,`value`) VALUES (1,4,5);
INSERT INTO `rating` (`user_id`,`tatoo_id`,`value`) VALUES (2,3,5);
INSERT INTO `rating` (`user_id`,`tatoo_id`,`value`) VALUES (4,1,3);

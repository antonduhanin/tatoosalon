
function check() {
    var password = document.getElementById("password").value;
    var confirmPassword = document.getElementById("confirmPassword").value;
    var login = document.getElementById("login").value;
    var confirmLogin = document.getElementById("confirmLogin").value;

    if (login == '' ||confirmLogin == ''
        || password == '' || confirmPassword == '') {
        document.getElementById('message').style.color = 'red';
        document.getElementById('message').innerHTML = 'field is empty';
        return false;
    }
    if (!(password == confirmPassword
        && login == confirmLogin)) {
        document.getElementById('message').style.color = 'red';
        document.getElementById('message').innerHTML = 'not matching';
        return false;
    }

    return ValidateEmail(login);
}

function ValidateEmail(login) {
    var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if (login.match(mailformat)) {
        return true;
    }
    else {
        document.getElementById('message').style.color = 'red';
        document.getElementById('message').innerHTML = 'You have entered an invalid email address!';
        return false;
    }
}
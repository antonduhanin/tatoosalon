document.addEventListener("DOMContentLoaded", function (event) {
    var indexNotNull = true;
    var index = 0;
    while (indexNotNull) {
        var name = "tattoo" + index;
        if (document.getElementsByName(name).length === 0) {
            indexNotNull = false;
        } else {
            if (document.getElementsByName(name)[0].getAttribute('src').length < 24) {
                document.getElementsByName(name)[0].src="../../image/empty.jpg";
            }
            document.getElementsByName(name)[0].hidden = false;
        }
        index++;
    }
});
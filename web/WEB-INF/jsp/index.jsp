<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Index</title>
    <!-- Bootstrap core CSS -->
    <link href="../../css/bootstrap/bootstrap.min.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="../../css/bootstrap/shop-homepage.css" rel="stylesheet">
    <!-- Bootstrap core JavaScript -->
    <script src="../../js/jquery/jquery.min.js"></script>
    <script src="../../js/bootstrap/js/bootstrap.bundle.min.js"></script>


    <script src="../../js/index.js"></script>
    <link href="../../css/index.css">
</head>
<body>
<!-- Navigation -->
<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
    <div class="container">
        <a class="navbar-brand" href="/index.html">Tattoo Salon</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="/index.html">Home
                        <span class="sr-only">(current)</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/tattoo.html">Add</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Services</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/${loginState}.html">${loginState}</a>
                </li>
            </ul>
        </div>
    </div>
</nav>

<!-- Page Content -->
<div class="container">
    <!-- Page Heading -->
    <h1 class="my-4">Bring Your Idea
        <small>to Life.</small>
    </h1>


    <div class="row">

        <c:forEach var="elem" items="${tattooList}" varStatus="loop">
            <div class="col-lg-4 col-md-6 mb-4">
                <div class="card h-100">
                    <c:forEach var="image" items="${elem.imageList}">
                        <img name="tattoo${loop.index}" hidden src="data:image/jpeg;base64,${image.base64Encoded}" alt="">
                    </c:forEach>
                    <div class="card-body">
                        <h4 class="card-title">
                            <c:out value="${elem.name}"/>
                        </h4>
                        <h5><c:out value="${elem.price}"/></h5>
                        <label for="averageMark">average mark</label>
                        <p id="averageMark">
                            <c:out value="${elem.ratingStatistics.average}"/>
                        </p>
                    </div>
                    <div class="card-footer">
                        <form action="/rate.html" method="post">
                            <input type="hidden" name="tattooId" value="${elem.id}">
                            <label for="select">Choose your mark</label>
                            <select class="form-control" id="select" name="rateValue">
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                            </select>
                            <br>
                            <input type="submit" class="btn btn-light" value="to rate"/>
                        </form>
                    </div>
                </div>
            </div>

        </c:forEach>
    </div>

    <%-- <div class="row">
     <c:forEach  var="elem" items="${tattooList}">

             <div class="card h-100">

                     <div class="card-body">
                         <h4 class="card-title">
                                 <c:out value="${elem.name}"/>
                         </h4>
                             <c:forEach var="image" items="${elem.imageList}">
                                 <p>
                                     <img alt="Lights" style="width:100%" class="card-img-top" id="imageTattoo" src="data:image/jpeg;base64,${image.base64Encoded}">
                                 </p>
                             </c:forEach>
                         <p class="card-text">
                             <label for="price">price</label>
                             <p id="price">
                                 <c:out value="${elem.price}"/>
                             </p>

                             <label for="averageMark">average mark</label>
                             <p id="averageMark">
                                 <c:out value="${elem.ratingStatistics.average}"/>
                             </p>

                         <form action="/rate.html" method="post">
                             <input type="hidden" name="tattooId" value="${elem.id}">
                             <label for="select">Choose your mark</label>
                             <select class="form-control" id="select" name="rateValue">
                                 <option value="1">1</option>
                                 <option value="2">2</option>
                                 <option value="3">3</option>
                                 <option value="4">4</option>
                                 <option value="5">5</option>
                             </select>
                             <br>
                             <input type="submit" class="btn btn-light" value="to rate"/>
                         </form>
                     </div>
             </div>
     </c:forEach>
     </div>--%>
</div>

<!-- Footer -->
<footer class="py-5 bg-dark">
    <div class="container">
        <p class="m-0 text-center text-white">Copyright &copy; TATTOO SALON 2018</p>
    </div>
</footer>
</body>

</html>

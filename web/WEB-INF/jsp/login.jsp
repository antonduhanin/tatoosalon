<%--
  Created by IntelliJ IDEA.
  User: ANTON
  Date: 24/09/2018
  Time: 20:08
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <link rel="stylesheet" type="text/css" href="../../css/loginStyle.css">
    <title>Login</title>
    <meta charset="utf-8">
    <title>Index</title>
    <!-- Bootstrap core CSS -->
    <link href="../../css/bootstrap/bootstrap.min.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="../../css/bootstrap/shop-homepage.css" rel="stylesheet">
    <!-- Bootstrap core JavaScript -->
    <script src="../../js/jquery/jquery.min.js"></script>
    <script src="../../js/bootstrap/js/bootstrap.bundle.min.js"></script>
</head>
<body>
<!-- Navigation -->
<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
    <div class="container">
        <a class="navbar-brand" href="/index.html">Tattoo Salon</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="/index.html">Home
                        <span class="sr-only">(current)</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/tattoo.html">Add</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Services</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/${loginState}.html">${loginState}</a>
                </li>
            </ul>
        </div>
    </div>
</nav>
<br>

<div class="container">
    <form  var="loginUrl" method="post" >

        <label for="login"><b>Login</b></label>
        <input type="text"  id="login" name="login"
               required>

        <label  for="password"><b>Password</b></label>
        <input type="password"  id="password" name="password" required>
        <c:if test="${not empty message}">
            <span  class="message">${message}</span>
        </c:if>
        <button type="submit">Sign It</button>
        <p class="message">Not registered?
            <a href="/registration.html">Create an account</a></p>
    </form>
</div>

<!-- Footer -->
<footer class="py-5 bg-dark">
    <div class="container">
        <p class="m-0 text-center text-white">Copyright &copy; TATTOO SALON 2018</p>
    </div>
</footer>

</body>
</html>

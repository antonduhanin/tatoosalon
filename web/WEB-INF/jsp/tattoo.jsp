<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Index</title>

    <!-- Bootstrap core CSS -->
    <link href="../../css/bootstrap/bootstrap.min.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="../../css/bootstrap/shop-homepage.css" rel="stylesheet">
    <!-- Bootstrap core JavaScript -->
    <script src="../../js/jquery/jquery.min.js"></script>
    <script src="../../js/bootstrap/js/bootstrap.bundle.min.js"></script>
</head>
<body>

<!-- Navigation -->
<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
    <div class="container">
        <a class="navbar-brand" href="/index.html">Tattoo Salon</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="/index.html">Home
                        <span class="sr-only">(current)</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/tattoo.html">Add</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Services</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/${loginState}.html">${loginState}</a>
                </li>
            </ul>
        </div>
    </div>
</nav>

<br>
<div class="container">
    <form action="/tattoo.html" enctype='multipart/form-data' method="post">
        <div class="form-group">
            <label for="nameTattoo">name of tattoo</label>
            <input type="text" class="form-control" name="nameTattoo" id="nameTattoo" required>
        </div>

        <c:if test="${not empty roleAdmin}">
            <div class="form-group">
                <label for="price">price of tattoo</label>
                <input type="number" id="price" name="price" value="1000" min="0" step="0.01" data-number-to-fixed="2" data-number-stepfactor="100" class="currency" />
            </div>

            <div class="form-group">
                <label for="state">state of tattoo</label>
                <select name="state" class="form-control" id="state">
                    <option>published</option>
                    <option>approved</option>
                    <option>wait</option>
                </select>
            </div>
        </c:if>

        <div class="form-group">
            <label>image for tattoo</label>
            <input type="file" name="uploadImage1" class="form-control-file" id="uploadImage1">
            <input type="file" name="uploadImage2" class="form-control-file" id="uploadImage2">
            <input type="file" name="uploadImage3" class="form-control-file" id="uploadImage3">
        </div>
        <button type="submit" class="btn btn-success">add tattoo</button>
    </form>

    <c:if test="${not empty message}">
        <span  class="message">${message}</span>
    </c:if>
</div>


<!-- Footer -->
<footer class="py-5 bg-dark">
    <div class="container">
        <p class="m-0 text-center text-white">Copyright &copy; TATTOO SALON 2018</p>
    </div>
</footer>

</body>
</html>